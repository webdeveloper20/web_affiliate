<?php
session_start();
include_once '../../Controller/productController.php';

$products = new Product_c();
if (isset($_POST['id'])) {
    $id = (int)$_POST['id'];
    $product = $products->getProductId_c($id);
    if (!isset($_SESSION['cart']) || empty($_SESSION['cart'])) {
        // nếu giỏ hàng ko tồn tại session cart hoặc giỏ hàng trống
        $_SESSION['cart'][$id] = $product;
        $_SESSION['cart'][$id]['qty'] = 1;
    } else {
        if (array_key_exists($id, $_SESSION['cart'])) {
            $_SESSION['cart'][$id]['qty'] += 1;
        } else {
            $_SESSION['cart'][$id] = $product;
            $_SESSION['cart'][$id]['qty'] = 1;
        }
    }
    echo "<div class='animate__animated animate__zoomInDown' style='animation-duration: 0.5s;color:red;font-weight:bold;box-shadow: 0px 1px 6px 0px #F5F5ED; border-radius: 10px; padding:10px;background-color: #F5F5ED;'>
    <p style='color:green'>Đã thêm vào giỏ hàng! </p><a href='index.php?page=home&method=cart' style='background-color: #FE980F;color:white;margin-left:30px;border-radius: 3px; padding : 5px;'>Xem ngay</a>
</div>";
}
?>