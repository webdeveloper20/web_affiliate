﻿<?php
session_start();
ob_start();
if (!isset($_SESSION['user_id'])) {
    header("Location: ./login.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include_once 'Views/layout/head.php';
    ?>
</head>

<body>

    <!-- Begin page -->
    <div id="wrapper">


        <!-- Topbar Start -->
        <div class="navbar-custom">
            <?php
            include_once 'Views/layout/navbar-custom.php';
            ?>
        </div>
        <!-- end Topbar -->
        <!-- ========== Left Sidebar Start ========== -->
        <div class="left-side-menu">
            <?php
            include_once 'Views/layout/left-side-menu.php';
            ?>
        </div>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">
                <div class="container-fluid">
                    <!-- noti -->
                    <?php
                    if (isset($_SESSION['noti']) && $_SESSION['noti'] == 1) {
                    ?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Thông báo:</strong> Thêm mới thành công
                        </div>
                    <?php
                    } elseif (isset($_SESSION['noti']) && $_SESSION['noti'] == 2) {
                    ?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Thông báo:</strong> Sửa thành công
                        </div>
                    <?php
                    } elseif (isset($_SESSION['noti']) && $_SESSION['noti'] == 3) {
                    ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Thông báo:</strong> Xóa thành công
                        </div>
                    <?php
                    }
                    unset($_SESSION['noti']);
                    ?>
                    <!-- --------------------------------------------- -->
                    <?php
                    if (isset($_GET['page'])) {
                        $page = $_GET['page'];
                    } else {
                        $page = 'admin';
                    }

                    switch ($page) {
                        case 'admin':
                            include_once 'Controller/Admin_c.php';
                            $shop = new Admin_c();
                            $shop->Admin_c();
                            break;
                            
                        case 'logout':
                            include_once 'views/pages/logout.php';
                            break;
                        default:
                            echo "<h4 style='color: red;'>ERROR 404, trang không tồn tại <span><a href='index.php' style='color: blue;'>Quay lại</a></span></h4>";
                            break;
                    }
                    ?>
                </div>
            </div>
            <!-- end content -->

            <!-- Footer Start -->
            <footer class="footer">
                <?php
                include_once 'Views/layout/footer.php';
                ?>
            </footer>
            <!-- end Footer -->

        </div>

    </div>
    <!-- END wrapper -->


    <!-- Right Sidebar -->
    <div class="right-bar">
        <?php
        include_once 'Views/layout/right-bar.php';
        ?>
    </div>
    <!-- /Right-bar -->

    <!-- Right bar overlay-->
    <?php
    include_once 'Views/layout/script.php';
    ?>

</body>

</html>