<div class="slimscroll-menu">

    <!--- Sidemenu -->
    <div id="sidebar-menu">
        <ul class="metismenu" id="side-menu">

            <li class="menu-title">Admin</li>

            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <i class="ion-md-speedometer"></i>
                    <span> Thống kê </span>
                    <!-- <span class="badge badge-info badge-pill float-right"> 3 </span> -->
                </a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li><a href="index.php">Dashboard</a></li>
                    <!-- <li><a href="dashboard-2.html">Dashboard 2</a></li>
                    <li><a href="dashboard-3.html">Dashboard 3</a></li> -->
                </ul>
            </li>

            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <i class="ion-md-basket"></i>
                    <span> Quản trị sản phẩm </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="nav-second-level" aria-expanded="false">

                    <li><a href="index.php?page=admin&method=add-product">Thêm sản phẩm</a></li>
                    <li><a href="index.php?page=admin&method=list-product">Danh sách sản phẩm</a></li>
                </ul>
            </li>

            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <i class="ion-ios-apps"></i>
                    <span> Quản trị danh mục </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="nav-second-level" aria-expanded="false">
                    <!-- <li><a href="components-grid.html">Thêm danh mục</a></li> -->
                    <li><a href="index.php?page=admin&method=list-cate">Danh sách danh mục</a></li>
                </ul>
            </li>

            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <i class="ion-md-speedometer"></i>
                    <span> Quản trị chương trình </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li><a href="index.php?page=admin&method=add_program">Thêm chương trình</a></li>
                    <li><a href="index.php?page=admin&method=list_program">Danh sách chương trình</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <i class="ion-md-pie"></i>
                    <span> Quản trị User </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li><a href="index.php?page=admin&method=list-member">Danh sách member</a></li>
                </ul>
            </li>

            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <i class="ion-md-mail"></i>
                    <span> Quản trị affiliate </span>
                    <span class="badge badge-warning badge-pill float-right">12</span>
                </a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li><a href="index.php?page=admin&method=affiliate_program">Danh sách affiliate_pro</a></li>
                    <li><a href="index.php?page=admin&method=listSaleAffiliate">Danh sách Sales_affiliate</a></li>
                </ul>
            </li>

            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <i class="ion-md-map"></i>
                    <span> Quản trị đơn hàng </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li><a href="index.php?page=admin&method=list-order"> Danh sách đơn hàng</a></li>
                </ul>
            </li>
        </ul>

    </div>
    <!-- End Sidebar -->

    <div class="clearfix"></div>

</div>
<!-- Sidebar -left -->