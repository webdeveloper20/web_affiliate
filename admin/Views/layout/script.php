<div class="rightbar-overlay"></div>

<a href="javascript:void(0);" class="right-bar-toggle demos-show-btn">
    <i class="mdi mdi-settings-outline mdi-spin"></i> &nbsp;Choose Demos
</a>

<!-- Vendor js -->
<script src="assets\js\vendor.min.js"></script>

<script src="assets\libs\moment\moment.min.js"></script>
<script src="assets\libs\jquery-scrollto\jquery.scrollTo.min.js"></script>
<script src="assets\libs\sweetalert2\sweetalert2.min.js"></script>

<!-- Chat app -->
<script src="assets\js\pages\jquery.chat.js"></script>

<!-- Todo app -->
<script src="assets\js\pages\jquery.todo.js"></script>

<!--Morris Chart-->
<script src="assets\libs\morris-js\morris.min.js"></script>
<script src="assets\libs\raphael\raphael.min.js"></script>

<!-- Sparkline charts -->
<script src="assets\libs\jquery-sparkline\jquery.sparkline.min.js"></script>

<!-- Dashboard init JS -->
<script src="assets\js\pages\dashboard.init.js"></script>

<!-- App js -->
<script src="assets\js\app.min.js"></script>

<!-- --------------------------->
<script src="assets\js\myJava.js"></script>