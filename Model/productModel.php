<?php  

	/**
	 * 
	 */
    include_once '../../config/myConfig.php';
 
	class Product_m extends Connect
	{
		
		function __construct(){
			parent::__construct(); // Gọi hàm __construct bên myConfig, luôn tồn tại $pdo để kết nối tới CSDL
		}
		

		// shop thêm vào cart
		public function getProductId($id){
			$sql = 'SELECT * FROM tbl_product WHERE id = :id';
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id", $id);
			$pre->execute();
			return $pre->fetch(PDO::FETCH_ASSOC);
		}

		
    }
?>