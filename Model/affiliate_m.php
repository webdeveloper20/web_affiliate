<?php  
	include_once 'config/myConfig.php';

	class Affiliate_m extends Connect
	{
		
        function __construct()
        {
			parent::__construct(); // Gọi hàm __construct bên myConfig, luôn tồn tại $pdo để kết nối tới CSDL
        }

        // danh sach chương trình
        protected function listAffiliate_m()
        {
            $sql = "SELECT * FROM `tbl_program` WHERE `status` = 1" ;
            $result = $this->pdo->prepare($sql);
            $result->execute();
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }

        // danh sách chương trình affiliate đã đăng ký 
        protected function listAffMember_m($affiliate_id)
        {
            $sql = "SELECT tbl_affiliate.affiliate_id ,  tbl_program.title,  tbl_program.rose,tbl_program.category , tbl_referal.id , tbl_referal.affiliate_code , tbl_referal.created_at , tbl_referal.status FROM tbl_affiliate , tbl_referal , tbl_program WHERE tbl_affiliate.affiliate_id = tbl_referal.affiliate_id AND tbl_program.program_id = tbl_referal.program_id AND tbl_affiliate.affiliate_id = :affiliate_id ";
            $result = $this->pdo->prepare($sql);
            $result->bindParam(":affiliate_id", $affiliate_id);
            $result->execute();
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }

        //đăng nhập
        protected function login_m($email,$password)
        {
            $sql = "SELECT * FROM `tbl_affiliate` WHERE `email` = :email AND `password` = :password";
            $result = $this->pdo->prepare($sql);
            $result->bindParam(":email", $email);
            $result->bindParam(":password", $password);
            $result->execute();
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }

        //check tài khoản tồn tại 
        protected function checkAccAffiliate($email){
            $sql = "SELECT * FROM `tbl_affiliate` WHERE `email` = :email";
            $result = $this->pdo->prepare($sql);
            $result->bindParam(":email", $email);
            $result->execute();
            return $result->fetchAll(PDO::FETCH_ASSOC);

        }

        // đăng ký tài khoản affiliate
        protected function register_m($name, $email, $phone, $password, $address)
        {
            $sql = "INSERT INTO `tbl_affiliate`( `name`, `email`, `phone`, `password`, `address`) VALUES (:name, :email, :phone, :password, :address)";
            $result = $this->pdo->prepare($sql);
            $result->bindParam(":name", $name);
            $result->bindParam(":email", $email);
            $result->bindParam(":phone", $phone);
            $result->bindParam(":password", $password);
            $result->bindParam(":address", $address);
            $result->execute();
        }

        //check referal tồn tại 
        protected function checkReferal_m($affiliate_id, $program_id)
        {
            $sql = "SELECT `id`, `affiliate_id`, `program_id`, `affiliate_code`, `status`, `created_at` FROM `tbl_referal` WHERE `affiliate_id` = :affiliate_id AND `program_id` = :program_id";
            $result = $this->pdo->prepare($sql);
            $result->bindParam(":affiliate_id", $affiliate_id);
            $result->bindParam(":program_id", $program_id);
            $result->execute();
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }

        // affiliate đăng ký chương trình
        protected function addAffiliate_m($affiliate_id, $program_id, $affiliate_code)
        {
            $sql = "INSERT INTO `tbl_referal`(`affiliate_id`, `program_id`, `affiliate_code`) VALUES (:affiliate_id, :program_id, :affiliate_code)";
            $result = $this->pdo->prepare($sql);
            $result->bindParam(":affiliate_id", $affiliate_id);
            $result->bindParam(":program_id", $program_id);
            $result->bindParam(":affiliate_code", $affiliate_code);
            $result->execute();
        }

        // xóa referal
        protected function delReferal_m($id)
        {
            $sql = "DELETE FROM `tbl_referal` WHERE id = :id";
            $pre = $this->pdo->prepare($sql);
            $pre->bindParam(":id", $id);
            $pre->execute();
        }

        // chi tiết doanh số bán hàng affiliate
        protected function listSaleAffDetail_m($affiliate_id)
        {
            $sql = "SELECT tbl_member.name_member as 'nameMember' , tbl_product.name as 'nameProduct' , tbl_detail_order.price , tbl_detail_order.quantity , tbl_detail_order.total , tbl_affiliate.name as 'nameAffiliate' , tbl_program.title as 'nameProgram' , tbl_program.rose , tbl_program.category  , tbl_order_referal.status , tbl_order_referal.id as 'orderReferal_id'
            FROM tbl_member , tbl_product , tbl_detail_order , tbl_affiliate , tbl_program , tbl_order_referal , tbl_order , tbl_referal
            WHERE tbl_order.id_member = tbl_member.id_member 
            AND tbl_detail_order.id_product = tbl_product.id 
            AND tbl_order_referal.order_id = tbl_order.id_order
            AND tbl_order_referal.order_id = tbl_detail_order.id_order
            AND tbl_order_referal.referal_id = tbl_referal.id
            AND tbl_referal.affiliate_id = tbl_affiliate.affiliate_id
            AND tbl_referal.program_id = tbl_program.program_id AND tbl_referal.affiliate_id = :affiliate_id";
            $result = $this->pdo->prepare($sql);
            $result->bindParam(":affiliate_id", $affiliate_id);
            $result->execute();
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }
    }
?>
