<?php  

	/**
	 * 
	 */
    include_once 'config/myConfig.php';
 
	class Product_m extends Connect
	{
		
		function __construct(){
			parent::__construct(); // Gọi hàm __construct bên myConfig, luôn tồn tại $pdo để kết nối tới CSDL
		}
		// -----------------------------------------------------------------------------------------------------------
		// Trang Home
		// Lấy sản phẩm cho trang Home
		public function trendProduct_m(){
			$sql = 'SELECT * FROM tbl_product ORDER BY RAND() LIMIT 6';
			$pre = $this->pdo->prepare($sql); 
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}

		// shop thêm vào cart
		public function getProductId($id){
			$sql = 'SELECT * FROM tbl_product WHERE id = :id';
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id", $id);
			$pre->execute();
			return $pre->fetch(PDO::FETCH_ASSOC);
		}

		// -----------------------------------------------------------------------------------------------------------
		// Trang Shop
		// public function getProduct_m(){
		// 	$sql = 'SELECT * FROM tbl_product ORDER BY tbl_product.id DESC ';
		// 	$pre = $this->pdo->prepare($sql);
		// 	$pre->execute();
		// 	return $pre->fetchAll(PDO::FETCH_ASSOC);
		// }

		// Trang shop có phân trang
		public function getProduct_m($pages, $record){
			$from = ($pages * $record) - $record ;

			$sql = "SELECT * FROM tbl_product 
					ORDER BY tbl_product.id DESC lIMIT $from, $record";
			$pre = $this->pdo->prepare($sql);
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}

		// Đếm số bản ghi trong banrhg tbl_member
		public function countMember_m(){
			$sql = "SELECT COUNT(id) as 'total' FROM tbl_product";
			$pre = $this->pdo->prepare($sql);
			$pre->execute();
			$count = $pre->fetch(PDO::FETCH_ASSOC);
			return $count['total'];
		}

		// Lấy danh mục sản phẩm
		public function getCate_m(){
			$sql = 'SELECT * FROM tbl_cate';
			$pre = $this->pdo->prepare($sql); 
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}

		// Lấy sản phẩm theo danh mục
		public function getCateShop_m($id){
			$sql = 'SELECT * FROM tbl_cate,tbl_product WHERE tbl_cate.id_cate = tbl_product.id_cate AND tbl_cate.id_cate = :id ';
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id", $id);
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}

		// Lấy thương hiệu
		public function getBrand_m(){
			$sql = 'SELECT * FROM tbl_brand';
			$pre = $this->pdo->prepare($sql); 
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}

		// Lấy sản phẩm theo thương hiệu
		public function getBrandShop_m($id){
			$sql = 'SELECT * FROM tbl_brand,tbl_product WHERE tbl_brand.id_brand = tbl_product.id_brand AND tbl_brand.id_brand = :id ';
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id", $id);
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}
		// -----------------------------------------------------------------------------------------------------------
		// Giỏ hàng
		public function addMember_m($name_member, $phone_member, $email_member, $addres, $password){
			$sql = "INSERT INTO tbl_member(name_member, phone_member, email_member, addres, password) 
			VALUES (:name_member, :phone_member, :email_member, :addres, :password)";
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":name_member", $name_member);
			$pre->bindParam(":phone_member", $phone_member);
			$pre->bindParam(":email_member", $email_member);
			$pre->bindParam(":addres", $addres);
			$pre->bindParam(":password", $password);
			return $pre->execute();
		}

		// Thêm đơn hàng
		public function addOrder_m($id_member, $note){
			$sql = "INSERT INTO tbl_order(id_member, note) 
			VALUES (:id_member, :note)";
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id_member", $id_member);
			$pre->bindParam(":note", $note);
			return $pre->execute();
		}
		
		// Thêm vào bảng chi tiết đơn hàng
		public function addOrderDetail_m($id_order, $id_product, $quantity, $price, $total){
			$sql = "INSERT INTO tbl_detail_order(id_order, id_product, quantity, price, total) 
			VALUES (:id_order, :id_product, :quantity, :price, :total)";
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id_order", $id_order);
			$pre->bindParam(":id_product", $id_product);
			$pre->bindParam(":quantity", $quantity);
			$pre->bindParam(":price", $price);
			$pre->bindParam(":total", $total);
			return $pre->execute();
		}
		
		// -----------------------------------------------------------------------------------------------------------
		// Chi tiết sản phẩm : product_detail.php
		public function getProductDetailId_m($id){
			$sql = 'SELECT * FROM tbl_cate,tbl_product,tbl_brand 
			WHERE tbl_cate.id_cate = tbl_product.id_cate AND tbl_product.id_brand = tbl_brand.id_brand AND tbl_product.id = :id';
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id", $id);
			$pre->execute();
			return $pre->fetch(PDO::FETCH_ASSOC);
		}
		
		// -----------------------------------------------------------------------------------------------------------
        // affiliate
        //check bảng referal 
        protected function checkReferal($affiliate_id,$program_id,$affiliate_code)
        {
            $sql = "SELECT * FROM `tbl_referal` WHERE `affiliate_id` = :affiliate_id AND `program_id` = :program_id AND `affiliate_code` = :affiliate_code AND `status` = 1";
            $pre = $this->pdo->prepare($sql);
            $pre->bindParam(":affiliate_id", $affiliate_id);
            $pre->bindParam(":program_id", $program_id);
            $pre->bindParam(":affiliate_code", $affiliate_code);
			$pre->execute();
			return $pre->fetch(PDO::FETCH_ASSOC);
        }

        // check bảng program
        protected function checkProgram($program_id)
        {
            $sql = "SELECT * FROM `tbl_program` WHERE `program_id` = :program_id  AND `status` = 1";
            $pre = $this->pdo->prepare($sql);
            $pre->bindParam(":program_id", $program_id);
            $pre->execute();
			return $pre->fetch(PDO::FETCH_ASSOC);
        }

        // check bảng affiliate
        protected function checkAffiliate($affiliate_id)
        {
            $sql = "SELECT * FROM `tbl_affiliate` WHERE `affiliate_id` = :affiliate_id AND `status` = 1";
            $pre = $this->pdo->prepare($sql);
            $pre->bindParam(":affiliate_id", $affiliate_id);
            $pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
        }

        //thêm vào bảng referal_order
        public function addOrderReferal($order_id, $referal_id)
        {
            $sql ="INSERT INTO `tbl_order_referal`(`order_id`, `referal_id`) VALUES (:order_id, :referal_id)";
            $pre = $this->pdo->prepare($sql);
			$pre->bindParam(":order_id", $order_id);
            $pre->bindParam(":referal_id", $referal_id);
            $pre->execute();
        }

        // lấy url
        protected function getCurURL()
        {
            // Kiểm tra xem giao thức web là http hay https
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
                $pageURL = "https://";
            } else {
                $pageURL = 'http://';
            }

            // Lấy url hiện tại, cái lúc trước bị thiếu cái $_SERVER["REQUEST_URI"];
            if (isset($_SERVER["SERVER_PORT"]) && $_SERVER["SERVER_PORT"] != "80") {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
            } else {
                $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            }
            return $pageURL;
        }
    }
?>