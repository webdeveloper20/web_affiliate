<?php
ob_start();
session_start();
// session_destroy();
if (isset($_GET['affiliate']) && !empty($_GET['affiliate'])) {
    $affiliate = $_GET['affiliate'];
    $arrCookie = array();
    $arrCookie[] = explode('.', $affiliate);
    setcookie('program_id', $arrCookie[0][0], time() + 604800);
    setcookie('affiliate_id', $arrCookie[0][1], time() + 604800);
    setcookie('affiliate_code', $affiliate, time() + 604800);
}
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 'home';
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include_once "Views/layout/head.php";
    ?>
</head>
<!--/head-->

<body style="position: relative;">
<div class="noti-cart" style="position: absolute;z-index: 10; top:300px; left:20px; position: fixed;">

</div>
    <header id="header">
        <!--header-->
        <?php
        if ($page != 'affiliate') {
            include_once "Views/layout/header.php";
        }
        ?>
    </header>
    <!--/header-->
    
	<!--slider-->
    <!-- <section id="slider">
        <?php if ($page == 'home' &&  isset($_GET['method']) && $_GET['method'] != 'home') {
        } else {
            include_once "Views/layout/slider.php";
        }
        ?>
	</section> -->
    <!--/slider-->


    <div class="modal fade modal-sm" id="modal-add-cart">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 300px; height: 125px">
                <div class="modal-body">
                    <h4 class="modal-title">Đã thêm vào giỏ hàng</h4>
                    <button type="button" class="btn btn-success"><a href="index.php?page=home&method=cart">Vào Giỏ Hàng</a></button>
                    <!-- <div class="alert alert-success">
                        <button><a href="index.php?page=home&method=cart">Vào Giỏ Hàng</a></button>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    <?php
    if (isset($_SESSION['notiCart']) && $_SESSION['notiCart'] == 1) {
    ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Thông báo:</strong> Đã được thêm vào giỏ hàng!
            <a href="index.php?page=home&method=cart"><button>Vào Giỏ Hàng</button></a>
        </div>
    <?php
    } elseif (isset($_SESSION['notiCart']) && $_SESSION['notiCart'] == 2) {
    ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Thông báo:</strong> Đặt hàng thành công, chúng tôi sẽ liên hệ với bạn
        </div>
    <?php
    }
    unset($_SESSION['notiCart']);
    ?>

    <?php
    switch ($page) {
        case 'home':
            include 'Controller/product_c.php';
            $phone = new Product_c();
            $phone->Product_c();
            break;
        case 'affiliate':
            include 'Controller/affiliate_c.php';
            $Affiliate = new Affiliate_c();
            $Affiliate->option();
            break;
        case 'info':
            include 'Controller/info_c.php';
            $phone = new Info_c();
            $phone->Info_c();
            break;
        default:
           header('Location:404.html');
            break;
    }
    // session_destroy();
    
    ?>
    <footer id="footer">
        <!--Footer-->
        <?php
        if ($page != 'affiliate') {
            include_once "Views/layout/footer.php";
        }
        ?>
    </footer>
    <!--/Footer-->
    <?php
    include_once "Views/layout/script.php";
    include_once "Views/page/plugin/messenger.php";
    ?>
</body>

</html>