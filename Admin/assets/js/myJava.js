/*-------------------------------------------------
		Admin : list_order.php 
--------------------------------------------------*/
// Show chi tiết đơn hàng trang list_order.php 
$(document).ready(function(){
	$(document).on('click', '.btn_detail_order', function(e){
		e.preventDefault();
		var id = $(this).val();
		// alert(id);
		// die();
	
		$.ajax({
			url 		: 'server/order/show_order_detail.php',
			type 		: 'GET',
			dataType 	: 'html',
			data 		: { id : id },
	
			success : function(data){
				$("#Modal_order .modal-body").html(data);
				// console.log(data);
			}
		});
	});
});