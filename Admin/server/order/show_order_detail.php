<?php
$ajax_flag = 1;

include_once '../../Controller/Admin_c.php';
$order = new Admin_c();
if (isset($_GET['id']) && $_GET['id'] > 0) {
    $id = (int)$_GET['id'];
    $rowtest = $order->getOrderDetailID_c($id);

?>
    
            <div class="table-responsive">
                <h4 style="text-align : center ;">Chi tiết đơn hàng</h4><br>
                <hr>
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Sản phẩm</th>
                            <th>Số lượng</th>
                            <th>Giá</th>
                            <!-- <th>Tổng tiền</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sum = 0;
                        $sumquantity = 0;
                        foreach ($rowtest as $value) {
                        ?>
                            <tr>
                                <td><?php echo $value['name']; ?></td>
                                <td><?php echo $value['quantity']; ?></td>
                                <td><?php echo number_format($value['price']);  ?> đ</td>
                            </tr>
                        <?php
                            $sum += $value['quantity'] * $value['price'];
                            $sumquantity += $value['quantity'];
                        }
                        ?>
                            <tr>
                                <td colspan="3" style="color: red; font-weight: bold;">
                                    Tổng số lượng: <?php echo $sumquantity ; ?>, Tổng tiền : <?php echo number_format($sum) ; ?> đ</td>
                            </tr>
                    </tbody>
                </table>
                <hr>
            </div>
        
<?php
}
?>