<?php  

	if(isset($ajax_flag)){
		include_once '../../Model/Admin_m.php';
	}else{
		include_once 'Model/Admin_m.php';
	}
	// include_once "Model/Admin_m.php";
	/**
	 * 
	 */
	class Admin_c extends Admin_m
	{
		private $one;

		function __construct()
		{
			$this->one = new Admin_m(); // Tự động chạy cái hàm __construct
		}
		public function Login_c()
        {
            if (isset($_POST['sm_login'])) {
				$email = $_POST['user'];
				$passw = md5($_POST['passw']) ;
				// echo $user ." ".$passw;

				$count = $this->one->Login_m($email, $passw);
		
				if (count($count) == 1) {
					// echo 11111;
					$_SESSION['user_id']   	= $count[0]['id'];
					$_SESSION['name'] 		= $count[0]['name'];
					header("Location: index.php");
				}else{
					// echo 2222;
					$errors = "Username or passord fail!";
				}
			}
			include_once "login.php";
        }

        //Quản trị pro gram
        public function addProgram_c()
        {
            include_once 'Views/pages/program/add_program.php';
        }

        public function listProgram_c()
        {
            $selectProgram = $this->one->selectProgram();
            include_once 'Views/pages/program/list_program.php';
        }

        public function listAffiliatePro_c()
        {
            $listAffiliatePro = $this->one->listAffiliatePro_m();
            include_once 'Views/pages/program/affiliate_pro.php';
        }

        public function listSaleAffiliate_c()
        {
            $listSaleAffiliate = $this->one->listSaleAffiliate_m();
            include_once 'Views/pages/program/listsale_affiliate.php';
		}
		
		// ----------------------------------------------------------------------------------------------
		// Xứ lý Ajax
		// Show chi tiết đơn hàng
		public function getOrderDetailID_c($id){
			return $this->one->getOrderDetailID_m($id);
		}

		// ----------------------------------------------------------------------------------------------

		public function Admin_c(){
			if (isset($_GET['method'])) {
				$method = $_GET['method'];
			}else{ 
				$method = 'home';
			}
			switch ($method) {
				case 'home':
					include_once "Views/pages/home.php";
					break;

				case 'list-product':

					$lP = $this->one->getProduct_m();
					// print_r($lP);

					include_once "Views/pages/product/list_product.php";
					break;

				case 'del-product':
					if (isset($_GET['id'])){
						$id = (int)$_GET['id'];
						$this->one->delProduct_m($id);	
					}
					header("Location:index.php?page=admin&method=list-product");
					break;
	
				case 'add-product':
					$cate = $this->one->getCate_m();
					$brand = $this->one->getBrand_m();
					if (isset($_POST['add_product'])){
						$name = $_POST['name'];
						$id_cate = $_POST['cate'];
						$id_brand = $_POST['brand'];
						$price = $_POST['price'];
						$quantity = $_POST['quantity'];
						$description = $_POST['description'];
                        $slug = $_POST['slug'];
                        $avatar = $_FILES['img'];
                        $img      = time().$this->one->convert_name($avatar['name']);
                        $tmp_name    = $avatar['tmp_name'];
                        // echo $img ; die();
                        move_uploaded_file($tmp_name, "../access/images/product/".$img); 

						$this->one->addProduct_m($name, $id_cate, $id_brand, $price, $img, $quantity, $description, $slug);	
                        header("Location:index.php?page=admin&method=list-product");
					}
					include_once 'Views/pages/product/add_product.php';
					break;

				case 'edit-product':
					if (isset($_GET['id'])) {
						$id = (int)$_GET['id'];
						$cate = $this->one->getCate_m();
						$productID = $this->one->getProductID_m($id);

						if (isset($_POST['update_product'])){
							$name = $_POST['name'];
							$id_cate = $_POST['cate'];
							$price = $_POST['price'];
							$img = $_POST['img'];
							$quantity = $_POST['quantity'];
							$description = $_POST['description'];
							$slug = $_POST['slug'];

							// echo $phone_name.$cate_id.$price.$img_id.$quantity.$description.$brand_id;
							// die();

							$this->one->editProduct_m($id, $name, $id_cate, $price, $img, $quantity, $description, $slug);	
							header("Location:index.php?page=admin&method=list-product");

						}
					}
					include_once 'Views/pages/product/edit_product.php';
					break;

				// ---------------------------------------------------------------------------------------
				/*----------------------------------------------
						Quản lý danh mục
				------------------------------------------------*/

				case 'list-cate':
					$lCate = $this->one->getCate_m();

					// ---------------------------------------------------------------------------------------
					// Add Category
					if (isset($_POST['add_cate'])){
						$name_cate = $_POST['name_cate'];

						// echo $name_cate;
						// die();

						$this->one->addCate_m($name_cate);	
						header("Location:index.php?page=admin&method=list-cate");
					}

					include_once "Views/pages/cate/list_cate.php";
					break;
				
				case 'del-cate':

					if (isset($_GET['id'])){
						$id = (int)$_GET['id'];
						$this->one->delCate_m($id);	
					}
					header("Location:index.php?page=admin&method=list-cate");
					break;

				case 'edit-cate':
					if (isset($_GET['id'])) {
						$id = (int)$_GET['id'];
						$cateID = $this->one->getCateID_m($id);

						if (isset($_POST['edit_cate'])){
							$name_cate = $_POST['name_cate'];

							// echo $name_cate;
							// die();

							$this->one->editCate_m($id, $name_cate);	
							header("Location:index.php?page=admin&method=list-cate");

						}
					}
					include_once 'Views/pages/cate/edit_cate.php';
					break;

				// ---------------------------------------------------------------------------------------
				// Quản lý khách hàng
				/*----------------------------------------------
						Quản lý khách hàng
				------------------------------------------------*/

				case 'list-member':
					if (isset($_POST['submit'])){
						$key = $_POST['key'];
						$lCus = $this->one->searchCustomer_m($key);
					}else{
						$lCus = $this->one->getMember_m();
					}

					// ---------------------------------------------------------------------------------------
					// Add Category
					if (isset($_POST['add_member'])){
						$name_member = $_POST['name_member'];
						$phone_member = $_POST['phone_member'];
						$email_member = $_POST['email_member'];
						$addres = $_POST['addres'];
						$password = $_POST['password'];

						// echo $name_member.$phone_member.$email_member.$addres.$password;
						// die();

						$this->one->addMember_m($name_member, $phone_member, $email_member, $addres, $password);	
						header("Location:index.php?page=admin&method=list-member");
					}

					include_once "Views/pages/user/list_member.php";
					break;
				case 'del-member':
					if (isset($_GET['id'])){
						$id = (int)$_GET['id'];
						$this->one->delMember_m($id);	
					}
					header("Location:index.php?page=admin&method=list-member");
					break;

				case 'edit-customer':
					if (isset($_GET['id'])) {
						$id = (int)$_GET['id'];
						// $customerID = $this->one->getCustomerID_m($id);

						if (isset($_POST['update_customer'])){
							$name_customer = $_POST['name_customer'];
							$phone_customer = $_POST['phone_customer'];
							$email_customer = $_POST['email_customer'];
							$addres = $_POST['addres'];
							$addate_birthdres = $_POST['date_birth'];

							// $this->one->editCustomer_m($id, $name_customer, $phone_customer, $email_customer, $addres, $date_birth);	
							header("Location:index.php?page=admin&method=list-customer");

						}
					}
					include_once 'pages/customer/edit_customer.php';
					break;
				// ---------------------------------------------------------------------------------------
				/*----------------------------------------------
						quản trị program 
				------------------------------------------------*/
				
				case 'list-order':
					if (isset($_POST['submit'])){
						$key = $_POST['key'];
						$lO = $this->one->searchOrder_m($key);
					}else{
						$lO = $this->one->getOrder_m();
					}

					// ---------------------------------------------------------------------------------------
					// Add Category
					if (isset($_POST['add_member'])){
						$name_member = $_POST['name_member'];
						$phone_member = $_POST['phone_member'];
						$email_member = $_POST['email_member'];
						$addres = $_POST['addres'];
						$password = $_POST['password'];

						// echo $name_member.$phone_member.$email_member.$addres.$password;
						// die();

						$this->one->addMember_m($name_member, $phone_member, $email_member, $addres, $password);	
						header("Location:index.php?page=admin&method=list-member");
					}

					if (!empty($_GET['id_order'])){
						$id = (int)$_GET['id_order'];
						
						$this->one->confirmOrder_m($id);
						$this->one->confirmDetail_m($id);
						
                        header("Location:index.php?page=admin&method=list-order");
					}

					include_once "Views/pages/order/list_order.php";
					break;

				case 'del-order':
					// Chưa xử lý được
					if (isset($_GET['id'])){
						$id = (int)$_GET['id'];
						// echo $id;
						// die();
						$this->one->delOrderDetail_m($id);	
						$this->one->delOrder_m($id);	
					}
					header("Location:index.php?page=admin&method=list-order");
					break;
				
				case 'edit-order':
					if (isset($_GET['id'])) {
						$id = (int)$_GET['id'];
						$stt = $this->one->getOrderStt_m();
						$orderID = $this->one->getOrderID_m($id);

						if (isset($_POST['update_order'])){
							$id = $_POST['order_id'];
							$order_stt = $_POST['order_stt'];
							$phone_customer = $_POST['phone_customer'];
							$name_customer = $_POST['name_customer'];
							$addres = $_POST['addres'];
							$note = $_POST['note'];

							$this->one->editOrder_m($id, $note, $order_stt);	
							// $this->one->editOrder_Customer_m($id, $order_stt);	
							header("Location:index.php?page=admin&method=list-order");

						}
					}
					include_once 'Views/pages/order/edit_order.php';
					break;

				// ---------------------------------------------------------------------------------------
				/*----------------------------------------------
						quản trị program 
				------------------------------------------------*/
                case 'add_program' : 
                    $this->addProgram_c() ;
                    if(isset($_POST['add_program'])){
                        $title = $_POST['title'];
                        $category = $_POST['category'];
                        $rose = $_POST['rose'];
                        $description = $_POST['description'];
                        if(!empty($title) && !empty($rose) && !empty($description)){
                            $this->one->addProgram_m($title, $rose, $description,$category);
                            header("Location:index.php?page=admin&method=list_program");
                        }
                    }
                    break ; 

                // danh sách chương trình
                case 'list_program' : 
                    $this->listProgram_c();
                    break ;
                // quản trị affiliate_program
                case 'affiliate_program' :
                    $this->listAffiliatePro_c();
                    if(isset($_GET['approved_id'])){
                        $id = (int)$_GET['approved_id'];
                        $this->one->approved_m($id);
                        header("Location:index.php?page=admin&method=affiliate_program");
                    }elseif(isset($_GET['stop'])){
                        $id = (int)$_GET['stop'];
                        $this->one->stop_m($id);
                        header("Location:index.php?page=admin&method=affiliate_program");
                    }
                    elseif(isset($_GET['delete'])){
                        $id = (int)$_GET['delete'];
                        $this->one->delReferal_m($id);
                        header("Location:index.php?page=admin&method=affiliate_program");
                    }
                    break ;
                // thống kê doanh số bán hàng affiliate
                case 'listSaleAffiliate' : 
                    $this->listSaleAffiliate_c() ;
                    // xác nhận tất cả doanh số (chuyển khoản or ...)
                    if(isset($_GET['confirm']) && $_GET['confirm'] == 1){
                        $this->one->confirmAll_m();
                        header("Location:index.php?page=admin&method=listSaleAffiliate");
                    }
                        // xác nhận một doanh số
                    elseif(isset($_GET['orderReferal_id']) && !empty(($_GET['orderReferal_id']))){
                        $id = $_GET['orderReferal_id'];
                        $this->one->confirmDetail_m($id);
                        header("Location:index.php?page=admin&method=listSaleAffiliate");
                    }
                    break ;
				default:
					echo "<h4 style='color: red;'>ERROR 404, trang không tồn tại <span><a href='index.php' style='color: blue;'>Quay lại</a></span></h4>";
					break;
			}
		}
    }
