<?php
session_start();

if (isset($_SESSION['user_id'])) {
    header("Location: ./index.php");
}

include_once 'Controller/Admin_c.php';
$login = new Admin_c();
$login->Login_c();
?>

<!doctype html>
<html lang="en">

<head>
    <title>Admin</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="shortcut icon" href="asset\images\favicon.ico">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="row" style="margin-top: 150px;">
            <div class="col-md-3"></div>
            <div class="col-md-6 col-md-push-3">
                <!-- Main -->
                <form action="" method="POST">
                    <legend>Thành Viên Đăng Nhập</legend>

                    <div class="form-group">
                        <label for="">Username</label>
                        <input type="email" required  name="user" class="form-control" value=" " placeholder="Nhập email của bạn">
                    </div>

                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" required name="passw" class="form-control" value="" placeholder="Nhập pass">
                    </div>

                    <button type="submit" name="sm_login" class="btn btn-primary">Đăng nhập</button>
                    <!-- <span>Nếu bạn chưa có tài khoản?
                        <a href="index.php?page=register" style="color: red;">Đăng ký</a></span> -->
                    <?php
                    if (isset($errors) && !empty($errors)) {
                    ?>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Warning: </strong> 
                        <?php echo $errors; ?>
                    </div>
                    <?php
                    }
                    ?>
                </form>
            </div>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>