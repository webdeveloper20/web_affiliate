<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Data Table</h4>
            <div class="page-title-right">
                <ol class="breadcrumb p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item active">Loại sản phẩm</li>
                </ol>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- end page title -->


<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-4">Tất cả loại sản phẩm</h4>
                <div>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".add-bs-example-modal-lg">Thêm loại sản phẩm</button>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <thead>
                            <tr>
                                <th colspan="1">
                                    <label>Search:<input type="text" id="search" name="search" class="form-control form-control-sm" placeholder="" aria-controls="datatable"></label>
                                </th>
                                <th colspan="2">
                                </th>
                                <th colspan="1">

                                    <select name="loc_status" aria-controls="datatable" class="custom-select custom-select-sm form-control form-control-sm loc_status">
                                        <option value="">Trạng thái</option>
                                        <option value="0">Wait</option>
                                        <option value="1">Confirm</option>
                                        <option value="2">Active</option>
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th>STT</th>
                                <th>ID</th>
                                <th>Tên danh mục</th>
                                <th>Ngày đăng</th>
                                <th>Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                // echo "<pre>";
                                // print_r($lCate);
                                // echo "</pre>";\
                                $stt = 0;
                                foreach ($lCate as $value){
                                    $stt += 1;
                            ?>
                            <tr>
                                <td><?php echo $stt; ?></td>
                                <td><?php echo $value['id_cate']; ?></td>
                                <td><?php echo $value['name_cate']; ?></td>
                                <td><?php echo $value['create_at']; ?></td>
                                <td>
                                    <!-- <button class="btn btn-primary">Edit</button> -->
                                    <a href="index.php?page=admin&method=edit-cate&id=<?php echo $value['id_cate']; ?>" >
                                        <button class="btn btn-success">Sửa</button>
                                    </a>
                                    <a href="index.php?page=admin&method=del-cate&id=<?php echo $value['id_cate']; ?>" onclick="return confirm('Bạn có thực sự muốn xóa sản phẩm này không? ');">
                                        <button class="btn btn-danger">Xóa</button>
                                    </a>
                                </td>
                            </tr>
                            <?php	
                                }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>
<!-- end row -->
<!-- ---------------------------------------------------------------------- -->
<!-- add-cate -->
<div class="modal fade add-bs-example-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
	<div class="modal-content" style="padding: 40px 40px;">
		<!-- id="forms_md" : id ở đây để xử lý AJAX để load lại form thôi còn ko ko tác dụng -->
      	<form action="" method="POST" role="form" id="forms_md"> 
			<legend>Thêm loại sản phẩm</legend>
		
			<div class="form-group">
				<label for="">Tên danh mục</label>
				<input type="text" required="" class="form-control" name="name_cate" placeholder="Nhập đầy đủ họ tên">
			</div>

			<button type="submit" name="add_cate" class="btn btn-primary">Thêm mới</button>
			<span id="noti_check_md"></span>
		</form>
    </div>
  </div>
</div>