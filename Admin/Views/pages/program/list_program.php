<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Data Table</h4>
            <div class="page-title-right">
                <ol class="breadcrumb p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item active">Chương trình</li>
                </ol>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- end page title -->


<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-4">Tất cả chương trình </h4>

                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <thead>
                            <tr>
                                <th colspan="2">
                                    <label>Search:<input type="text" id="search" name="search" class="form-control form-control-sm" placeholder="" aria-controls="datatable"></label>
                                </th>
                                <th colspan="4">
                                </th>
                                <th colspan="2">

                                    <select name="loc_status" aria-controls="datatable" class="custom-select custom-select-sm form-control form-control-sm loc_status">
                                        <option value="">Trạng thái</option>
                                        <option value="0">Wait</option>
                                        <option value="1">Confirm</option>
                                        <option value="2">Active</option>
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th>STT</th>
                                <th>Tên chương trình</th>
                                <th>Hoa hồng</th>
                                <th>Mô tả</th>
                                <th>Trạng thái</th>
                                <th>Ngày tạo</th>
                                <th colspan="2">Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $stt =0;
                                foreach ($selectProgram as $pro) {
                             ?>
                                <tr>
                                    <td><?=$stt+1?></td>
                                    <td><?=$pro['title']?></td>
                                    <td style="color:red;"><?php if($pro['category'] == 1){echo number_format($pro['rose'], 0, ',', '.')." VNĐ";}elseif($pro['category'] == 2){echo $pro['rose']."%";} ?></td>
                                    <td><?=$pro['description']?></td>
                                    <td><?php if($pro['status'] == 1){echo "online";}else{ echo "offline"; } ?></td>
                                    <td><?=$pro['created_at']?></td>
                                    <td><a href="" class="btn btn-success">Sửa</a></td>
                                    <td><a href="" class="btn btn-danger">Xóa</a></td>
                                </tr>
                             <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>