<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Data Table</h4>
            <div class="page-title-right">
                <ol class="breadcrumb p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item active">Chương trình</li>
                </ol>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- end page title -->


<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-4">Kết quả bán hàng affiliate </h4>

                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <thead>
                            <tr>
                                <th colspan="2">
                                    <label>Search:<input type="text" id="search" name="search" class="form-control form-control-sm" placeholder="" aria-controls="datatable"></label>
                                </th>
                                <th colspan="4">
                                </th>
                                <th colspan="2">

                                    <select name="loc_status" aria-controls="datatable" class="custom-select custom-select-sm form-control form-control-sm loc_status">
                                        <option value="">Trạng thái</option>
                                        <option value="0">Wait</option>
                                        <option value="1">Confirm</option>
                                        <option value="2">Active</option>
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th>STT</th>
                                <th>Tên khách hàng</th>
                                <th>Tên sản phẩm</th>
                                <th>Đơn giá</th>
                                <th>Số lượng</th>
                                <th>Tổng tiền</th>
                                <th>Tên Affiliate</th>
                                <th>Tên Pro</th>
                                <th>Hoa hồng</th>
                                <th>Tiền hoa hồng</th>
                                <th>Trạng thái</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $stt = 1;
                                $moneyReceived = $moneyNotReceived = 0;
                                foreach ($listSaleAffiliate as $listSale) {
                             ?>
                                <tr>
                                    <td><?=$stt++?></td>
                                    <td><?=$listSale['nameMember']?></td>
                                    <td><?=$listSale['nameProduct']?></td>
                                    <td><?= number_format($listSale['price'], 0, ',', '.')?></td>
                                    <td><?=$listSale['quantity']?></td>
                                    <td><?= number_format($listSale['total'], 0, ',', '.')?></td>
                                    <td><?=$listSale['nameAffiliate']?></td>
                                    <td><?=$listSale['nameProgram']?></td>
                                    <td style="color:blue;"><?php if($listSale['category'] == 2){ echo  $listSale['rose']."%" ; }
                                elseif($listSale['category'] == 1){echo  number_format($listSale['rose'], 0, ',', '.')." VNĐ";} ?></td>
                                    <td style="color:red;"><?php if ($listSale['category'] == 2) {
                                                            $totalOne = $listSale['total'] / 100 * $listSale['rose'];
                                                            echo number_format($totalOne, 0, ',', '.') . " VNĐ";
                                                        } elseif ($listSale['category'] == 1) {
                                                            $totalOne = $listSale['rose'];
                                                            echo  number_format($totalOne, 0, ',', '.') . " VNĐ";
                                                        } ?></td>
                                    <?php if($listSale['status'] == 0){
                                    ?>
                                    <td><a href="index.php?page=admin&method=listSaleAffiliate&orderReferal_id=<?=$listSale['orderReferal_id']?>">Chờ xác nhận</a></td>
                                    <?php
                                    }else{
                                    ?>
                                    <td style="color:green;">đã CK</td>
                                    <?php
                                    } ?>
                                    
                                </tr>
                             <?php
                                 if ($listSale['status'] == 0) {
                                     $moneyNotReceived += $totalOne;
                                 } else {
                                     $moneyReceived += $totalOne;
                                 }
                                }
                            ?>
                            <tr>
                                <td colspan="9" style="color:green; font-weight:bold;">
                                    Tổng tiền hoa hồng đã chuyển khoản cho khách
                                </td>
                                <td colspan="3"  style="color:red; font-weight:bold;">
                                <?php echo number_format($moneyReceived, 0, ',', '.') . " VNĐ"; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9"  style="color:green; font-weight:bold;">
                                    Tổng tiền hoa hồng chưa chuyển khoản cho khách
                                </td>
                                <td colspan="3"  style="color:red; font-weight:bold;">
                                <?php echo number_format($moneyNotReceived, 0, ',', '.') . " VNĐ"; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="12" style="text-align: center;">
                                     <a href="index.php?page=admin&method=listSaleAffiliate&confirm=1" onclick="return confirm('Bạn có muốn xác nhận tất cả không ?')" class="btn btn-primary">Xác nhận tất cả</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
