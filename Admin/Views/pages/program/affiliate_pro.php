<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Data Table</h4>
            <div class="page-title-right">
                <ol class="breadcrumb p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item active">Chương trình</li>
                </ol>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- end page title -->


<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-4">Danh sách chương trình affiliate đăng ký </h4>

                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <thead>
                            <tr>
                                <th colspan="2">
                                    <label>Search:<input type="text" id="search" name="search" class="form-control form-control-sm" placeholder="" aria-controls="datatable"></label>
                                </th>
                                <th colspan="4">
                                </th>
                                <th colspan="2">
                                    <select name="loc_status" aria-controls="datatable" class="custom-select custom-select-sm form-control form-control-sm loc_status">
                                        <option value="">Trạng thái</option>
                                        <option value="0">Wait</option>
                                        <option value="1">Confirm</option>
                                        <option value="2">Active</option>
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th>STT</th>
                                <th>Affiliate</th>
                                <th>Chương trình</th>
                                <th>Hoa hồng</th>
                                <th>Trạng thái</th>
                                <th>Ngày đăng ký</th>
                                <th colspan="2" style="text-align: center;">Tùy chọn</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $stt = 1;
                                foreach ($listAffiliatePro as $AffPro) {
                             ?>
                                <tr>
                                    <td><?=$stt++;?></td>
                                    <td><?=$AffPro['name']?></td>
                                    <td><?=$AffPro['title']?></td>
                                    <td style="color:red; font-weight: bold;"><?php if($AffPro['category'] == 2){ echo  $AffPro['rose']."%" ; }
                                elseif($AffPro['category'] == 1){echo  number_format($AffPro['rose'], 0, ',', '.')." VNĐ";} ?> </td>
                                    <td style="color:green;"><?php if($AffPro['status'] == 1){echo "Hoạt động";}else{ echo "Chờ duyệt"; } ?></td>
                                    <td><?=$AffPro['created_at']?></td>
                                    <?php if($AffPro['status'] == 0){
                                    ?>
                                    <td><a href="index.php?page=admin&method=affiliate_program&approved_id=<?=$AffPro['referal_id']?>" class="btn btn-primary">Phê duyệt</a></td>
                                    <?php
                                    }
                                    else{
                                    ?>
                                    <td><a href="index.php?page=admin&method=affiliate_program&stop=<?=$AffPro['referal_id']?>" class="btn btn-warning">Buộc dừng</a></td>
                                    <?php
                                    }
                                    ?>
                                    <td><a href="index.php?page=admin&method=affiliate_program&delete=<?=$AffPro['referal_id']?>" onclick="return confirm('Bạn có muốn xóa không ?')" class="btn btn-danger">Xóa</a></td>
                                </tr>
                             <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
