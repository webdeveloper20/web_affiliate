<form class="parsley-examples" method="POST" action="#">
    <legend>Thêm chương trình affiliate</legend>

    <div class="form-group">
        <label for="userName">Tên chương trình<span class="text-danger">*</span></label>
        <input type="text" name="title" parsley-trigger="change" required="" placeholder="nhập tên chương trình" class="form-control" id="userName">
    </div>
    <div class="form-group">
        <label for="">Loại hoa hồng<span class="text-danger"> *</span></label>
        <select name="category" required class="form-control" id="">
            <option value="1">Tiền mặt</option>
            <option value="2">Theo %</option>
        </select>
    </div>
    <div class="form-group">
        <label for="">Hoa hồng<span class="text-danger"> *</span></label>
        <input type="number" required class="form-control" name="rose" placeholder="">
    </div>
    <div class="form-group">
        <label for="">Mô tả<span class="text-danger"> *</span></label>
        <textarea name="description" class="form-control" id="" rows="5"></textarea>
    </div>
    <button type="submit" name="add_program" class="btn btn-primary">Thêm mới</button>
</form>