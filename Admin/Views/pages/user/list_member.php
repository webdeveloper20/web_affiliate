<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Data Table</h4>
            <div class="page-title-right">
                <ol class="breadcrumb p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item active">Khách hàng</li>
                </ol>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- end page title -->


<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-4">Tất cả khách hàng</h4>
                <div>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".add-cus-modal-lg">Thêm khách hàng</button>

		        </div>

                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <thead>
                            <tr>
                                <th colspan="1">
                                    <label>Search:<input type="text" id="search" name="search" class="form-control form-control-sm" placeholder="" aria-controls="datatable"></label>
                                </th>
                                <th colspan="4">
                                </th>
                                <th colspan="2">

                                    <select name="loc_status" aria-controls="datatable" class="custom-select custom-select-sm form-control form-control-sm loc_status">
                                        <option value="">Trạng thái</option>
                                        <option value="0">Wait</option>
                                        <option value="1">Confirm</option>
                                        <option value="2">Active</option>
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th>STT</th>
                                <th>ID</th>
                                <th>Tên khách hàng</th>
                                <th>Điện thoại</th>
                                <th>Email</th>
                                <th>Địa chỉ</th>
                                <th>Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                // echo "<pre>";
                                // print_r($lP);
                                // echo "</pre>";
                                $stt =0;
                                foreach ($lCus as $value){
                                    $stt += 1;
                            ?>
                            <tr>
                                <th scope="row"><?php echo $stt; ?></th>
                                <td><?php echo $value['id_member']; ?></td>
                                <td><?php echo $value['name_member']; ?></td>
                                <td><?php echo $value['phone_member']; ?></td>
                                <td><?php echo $value['email_member']; ?></td>
                                <td><?php echo $value['addres']; ?></td>
                                <td>
                                    <!-- <button class="btn btn-primary">Edit</button> -->
                                    <a href="index.php?page=admin&method=edit-member&id=<?php echo $value['id_member']; ?>" >
                                        <button class="btn btn-success">Sửa</button>
                                    </a>
                                    <a href="index.php?page=admin&method=del-member&id=<?php echo $value['id_member']; ?>" onclick="return confirm('Bạn có thực sự muốn xóa sản phẩm này không? ');">
                                        <button class="btn btn-danger">Xóa</button>
                                    </a>
                                </td>
                            </tr>
                            <?php	
                                }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>
<!-- end row -->
<!-- ---------------------------------------------------------------------- -->
<!-- add-customer -->
<div class="modal fade add-cus-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
	<div class="modal-content" style="padding: 40px 40px; background-color:#ccf2ff">
		<!-- id="forms_md" : id ở đây để xử lý AJAX để load lại form thôi còn ko ko tác dụng -->
      	<form action="" method="POST" role="form" id="forms_md"> 
			<legend>Thêm mới khách hàng</legend>
		
			<div class="form-group">
				<label for="">Tên khách hàng</label>
				<input type="text" required="" class="form-control" name="name_member" placeholder="Nhập đầy đủ họ tên">
			</div>

			<div class="form-group">
				<label for="">Điện thoại</label>
				<input type="number" required="" class="form-control" name="phone_member" placeholder="">
			</div>

			<div class="form-group">
				<label for="">Email</label>
				<input type="email" required="" class="form-control" name="email_member" placeholder="">
			</div>

			<div class="form-group">
				<label for="">Addres</label>
				<input type="text" required="" class="form-control" name="addres" placeholder="">
			</div>

			<div class="form-group">
				<label for="">Mật khẩu</label>
				<input type="password" required="" class="form-control" name="password" placeholder="">
			</div>

			<button type="submit" name="add_member" class="btn btn-success">Thêm mới</button>
			<span id="noti_check_md"></span>
		</form>
    </div>
  </div>
</div>
<!-- ---------------------------------------------------------------------- -->