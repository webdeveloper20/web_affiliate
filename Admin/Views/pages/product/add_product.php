<form class="parsley-examples" method="POST" action="" enctype="multipart/form-data">
    <legend>Thêm mới sản phẩm</legend>
	<div class="row">
		<div class="col-lg-7 col-md-6">
			<div class="form-group">
				<label for="userName">Tên sản phẩm<span class="text-danger">*</span></label>
				<input type="text" name="name" parsley-trigger="change" required="" placeholder="" class="form-control" id="userName">
			</div>
			<div class="form-group">
				<label for="">Giá sản phẩm<span class="text-danger"> *</span></label>
				<input type="number" required class="form-control" name="price" placeholder="">
			</div>
			<div class="form-group">
				<label for="">Hình ảnh<span class="text-danger"> *</span></label>
				<!-- <input type="file" required class="form-control" name="img" placeholder=""> -->
				<input type="text" required class="form-control" name="img" placeholder="">
			</div>
			
			<div class="form-group">
				<label for="">Số lượng<span class="text-danger"> *</span></label>
				<input type="number" required class="form-control" name="quantity" placeholder="">
			</div>
		</div>
		<div class="col-lg-5 col-md-6">
			<div class="form-group">
				<label>Danh mục</label>
				<select class="form-control" name="cate">
					<?php
					// $fac = getFaculty();
					foreach ($cate as $valCate) {
					?>
						<option value="<?php echo $valCate['id_cate']; ?>">
							<?php echo $valCate['name_cate']; ?>
						</option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="form-group">
				<label>Thương hiệu</label>
				<select class="form-control" name="brand">
					<?php
					// $fac = getFaculty();
					foreach ($brand as $valBrand) {
					?>
						<option value="<?php echo $valBrand['id_brand']; ?>">
							<?php echo $valBrand['brand_name']; ?>
						</option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="form-group">
				<label for="">Link<span class="text-danger"> *</span></label>
				<input type="text" required class="form-control" name="slug" placeholder="">
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="">Mô tả<span class="text-danger"> *</span></label>
		<!-- <input type="text" required class="form-control ckeditor" name="description" placeholder=""> -->
		<textarea class="form-control ckeditor" name="description" cols="30" rows="5" style="resize: none;"></textarea>
	</div>
	<button type="submit" name="add_product" class="btn btn-primary">Thêm mới</button>
</form>