<form class="parsley-examples" method="POST" action="#">
    <legend>Sửa thông tin sản phẩm</legend>

    <div class="form-group">
        <label for="userName">Tên sản phẩm<span class="text-danger">*</span></label>
        <input type="text" name="name" value="<?php echo $productID['name']; ?>" parsley-trigger="change" required="" placeholder="Enter user name" class="form-control" id="userName">
    </div>
    <div class="form-group">
		<label>Danh mục</label>
		<select class="form-control" name="cate">
			<?php
			// $fac = getFaculty();
			foreach ($cate as $valCate) {
			?>
                <option <?php if($productID['id_cate'] == $valCate['id_cate']) { echo "selected"; } ?> value="<?php echo $valCate['id_cate']; ?>">
					<?php echo $valCate['name_cate']; ?>
				</option>
			<?php
			}
			?>
		</select>
    </div>
    <div class="form-group">
		<label for="">Giá sản phẩm<span class="text-danger"> *</span></label>
		<input type="number" required value="<?php echo $productID['price']; ?>" class="form-control" name="price" placeholder="">
    </div>
    <div class="form-group">
		<label for="">Hình ảnh<span class="text-danger"> *</span></label>
		<input type="text" required value="<?php echo $productID['img']; ?>" class="form-control" name="img" placeholder="">
    </div>
    
    <div class="form-group">
		<label for="">Số lượng<span class="text-danger"> *</span></label>
		<input type="number" required value="<?php echo $productID['quantity']; ?>" class="form-control" name="quantity" placeholder="">
	</div>

	<div class="form-group">
		<label for="">Mô tả<span class="text-danger"> *</span></label>
		<!-- <input type="text" required class="form-control ckeditor" name="description" placeholder=""> -->
		<textarea class="form-control ckeditor" name="description" cols="30" rows="5" style="resize: none;"><?php echo $productID['description']; ?></textarea>
	</div>

    <div class="form-group">
		<label for="">Link<span class="text-danger"> *</span></label>
		<input type="text" required value="<?php echo $productID['slug']; ?>" class="form-control" name="slug" placeholder="">
    </div>

	<button type="submit" name="update_product" class="btn btn-primary">Cập nhật</button>
</form>