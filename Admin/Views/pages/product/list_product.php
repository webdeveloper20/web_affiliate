<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Data Table</h4>
            <div class="page-title-right">
                <ol class="breadcrumb p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item active">Sản phẩm</li>
                </ol>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- end page title -->


<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-4">Tất cả sản phẩm</h4>

                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <thead>
                            <tr>
                                <th colspan="2">
                                    <label>Search:<input type="text" id="search" name="search" class="form-control form-control-sm" placeholder="" aria-controls="datatable"></label>
                                </th>
                                <th colspan="4">
                                </th>
                                <th colspan="2">

                                    <select name="loc_status" aria-controls="datatable" class="custom-select custom-select-sm form-control form-control-sm loc_status">
                                        <option value="">Trạng thái</option>
                                        <option value="0">Wait</option>
                                        <option value="1">Confirm</option>
                                        <option value="2">Active</option>
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th>STT</th>
                                <th>ID</th>
                                <th>Tên sản phẩm</th>
                                <th>Hình ảnh</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Tên danh mục</th>
                                <th>Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $stt =0;
                                foreach ($lP as $value){
                                    $stt += 1;
    
                            ?>
                            <tr>
                                <th scope="row"><?php echo $stt; ?></th>
                                <td><?php echo $value['id']; ?></td>
                                <td><?php echo $value['name']; ?></td>
                                <td><img class="" src="../access/images/product/<?php echo $value['img']; ?>" style="width: 99px; height:99px" ></td>
                                <td><?php echo $value['price']; ?></td>
                                <td><?php echo $value['quantity']; ?></td>
                                <td><?php echo $value['name_cate']; ?></td>
                                <td>
                                    <!-- <button class="btn btn-primary">Edit</button> -->
                                    <a href="index.php?page=admin&method=edit-product&id=<?php echo $value['id']; ?>" >
                                        <button class="btn btn-success">Sửa</button>
                                    </a>
                                    <a href="index.php?page=admin&method=del-product&id=<?php echo $value['id']; ?>" onclick="return confirm('Bạn có thực sự muốn xóa sản phẩm này không? ');">
                                        <button class="btn btn-danger">Xóa</button>
                                    </a>
                                </td>
                            </tr>
                            <?php	
                                }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>
<!-- end row -->