<form action="" method="POST" role="form">
	<legend>Sửa thông tin đơn hàng</legend>

	<?php
	// echo "<pre>";
	// echo print_r($orderID);
	// echo "</pre>";
	// foreach ($orderID as $value) {
	?>
	<div class="row">
		<div class="col-lg-4 col-md-6">
			<div class="form-group">
				<label for="">Mã đơn hàng<span style="color: red;"> *</span></label>
				<input type="text" readonly value="<?php echo $orderID[0]['order_id']; ?>" class="form-control" name="order_id" placeholder="">
			</div>
			<div class="form-group">
				<label for="">Số điện thoại<span style="color: red;"> *</span></label>
				<input type="number" readonly required value="<?php echo $orderID[0]['phone_customer']; ?>" class="form-control" name="phone_customer" placeholder="">
			</div>
			<div class="form-group">
				<label>Trạng thái</label>
				<select class="form-control" name="order_stt">
				<?php
				foreach ($stt as $val) {
				?>
					<option <?php if($orderID[0]['order_stt'] == $val['status_id']) { echo "selected"; } ?> value="<?php echo $val['status_id']; ?>">
						<?php echo $val['status_name']; ?>
					</option>
				<?php
				}
				?>
					
				</select>
			</div>
		</div>
		<div class="col-lg-8 col-md-6">
			<div class="form-group">
				<label for="">Tên khách hàng<span style="color: red;"> *</span></label>
				<input type="text" readonly value="<?php echo $orderID[0]['name_customer']; ?>" class="form-control" name="name_customer" placeholder="">
			</div>
			<div class="form-group">
				<label for="">Địa chỉ nhận hàng<span style="color: red;"> *</span></label>
				<input type="text" readonly required value="<?php echo $orderID[0]['addres']; ?>" class="form-control" name="addres" placeholder="">
			</div>
			<div class="form-group">
				<label for="">Ngày đặt<span style="color: red;"> *</span></label>
				<input type="text" readonly value="<?php echo $orderID[0]['order_date']; ?>" class="form-control" name="order_date" placeholder="">
			</div>
		</div>
		<div class="col-lg-12 col-md-12">
			<div class="form-group">
				<label for="">Ghi chú</label>
				<textarea class="form-control ckeditor" name="note" cols="30" rows="5" style="resize: none;"><?php echo $orderID[0]['note']; ?></textarea>
			</div>
		</div>
	</div>
	<!-- <div class="form-group">
		<label for="">Mã đơn hàng<span style="color: red;"> *</span></label>
		<input type="text" readonly value="<?php echo $orderID[0]['order_id']; ?>" class="form-control" name="order_id" placeholder="">
	</div>

	<div class="form-group">
		<label for="">Tên khách hàng<span style="color: red;"> *</span></label>
		<input type="text" readonly value="<?php echo $orderID[0]['name_customer']; ?>" class="form-control" name="name_customer" placeholder="">
	</div>

	<div class="form-group">
		<label for="">Số điện thoại<span style="color: red;"> *</span></label>
		<input type="number" readonly required value="<?php echo $orderID[0]['phone_customer']; ?>" class="form-control" name="phone_customer" placeholder="">
	</div>

	<div class="form-group">
		<label for="">Địa chỉ nhận hàng<span style="color: red;"> *</span></label>
		<input type="text" readonly required value="<?php echo $orderID[0]['addres']; ?>" class="form-control" name="addres" placeholder="">
	</div>

	<div class="form-group">
		<label>Trạng thái</label>
		<select class="form-control" name="order_stt">
			<?php
			// $fac = getFaculty();
			// foreach ($cate as $valCate) {
			?>
			<option value="<?php echo $orderID[0]['order_stt']; ?>">
				<?php
				if ($orderID[0]['order_stt'] == 0) {
					echo "Đang xử lý";
				} else if ($orderID[0]['order_stt'] == 1) {
					echo "Đã xác nhận";
				} else if ($orderID[0]['order_stt'] == 2) {
					echo "Đang giao hàng";
				} else {
					echo "Đã hoàn thất";
				}
				?>
			</option>
			<option value="0">Đang xử lý</option>
			<option value="1">Đã xác nhận</option>
			<option value="2">Đang giao hàng</option>
			<option value="3">Đã hoàn tất</option>
			<?php
			// }
			?>
		</select>
	</div>

	<div class="form-group">
		<label for="">Ghi chú</label>
		<textarea class="form-control" name="note" id="" cols="30" rows="5" style="resize: none;"><?php echo $orderID[0]['note']; ?></textarea>
	</div>

	<div class="form-group">
		<label for="">Ngày đặt<span style="color: red;"> *</span></label>
		<input type="text" readonly value="<?php echo $orderID[0]['order_date']; ?>" class="form-control" name="order_date" placeholder="">
	</div> -->

	<?php
	// }
	?>

	<button type="submit" name="update_order" class="btn btn-primary">Cập nhật</button>

</form>