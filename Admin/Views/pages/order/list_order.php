<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Đơn Hàng</h4>
            <div class="page-title-right">
                <ol class="breadcrumb p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item active">Đơn hàng</li>
                </ol>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- end page title -->


<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-4">Danh sách đơn hàng</h4>

                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <thead>
                            <tr>
                                <th colspan="2">
                                    <!-- <label>Search:<input type="text" id="search" name="key" class="form-control form-control-sm" placeholder="" aria-controls="datatable"></label> -->

                                    <form action="" method="POST">
                                        <div class="form-group input-group">
                                            <label>Search:<input type="text" id="search" name="key" class="form-control form-control-sm" placeholder="" aria-controls="datatable"></label>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit" name="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </form>
                                </th>
                                <th colspan="4">
                                </th>
                                <th colspan="2">

                                    <select name="loc_status" aria-controls="datatable" class="custom-select custom-select-sm form-control form-control-sm loc_status">
                                        <option value="">Trạng thái</option>
                                        <option value="0">Wait</option>
                                        <option value="1">Confirm</option>
                                        <option value="2">Active</option>
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th>STT</th>
                                <th>Mã đơn hàng</th>
                                <th>Tên khách hàng</th>
                                <th>Địa chỉ</th>
                                <th>Điện thoại</th>
                                <th>Trạng thái</th>
                                <th>Ngày đặt</th>
                                <th>Chức năng</th>
                                <th>Chi tiết</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                                $stt =0;
                                foreach ($lO as $value){
                                    $stt += 1;
                            ?>
                            <tr>
                                <th scope="row"><?php echo $stt; ?></th>
                                <td><?php echo $value['id_order']; ?></td>
                                <td><?php echo $value['name_member']; ?></td>
                                <td><?php echo $value['addres']; ?></td>
                                <td><?php echo $value['phone_member']; ?></td>
                                <!-- <td> -->
                                    <!-- <?php if ($value['stt_order'] == 1) {
                                    ?>
                                        <span style="font-weight: bold; background-color: #fcf514 ; padding:5px ">Chờ xác nhận</span>
                                    <?php
                                    } else if ($value['stt_order'] == 2) {
                                    ?>
                                        <span style="font-weight: bold; background-color: #63e6e3; padding:5px ">Đang giao hàng</span>
                                    <?php
                                    } else if ($value['stt_order'] == 3) {
                                    ?>
                                        <span style="font-weight: bold; background-color: #3deb5a; padding:5px ">Đã hoàn tất</span>
                                    <?php
                                    } else {
                                    ?>
                                        <span style="font-weight: bold; background-color: #eb3d54 ; padding:5px ">Đã hủy</span>
                                    <?php
                                    }
                                    ?> -->
                                <!-- </td> -->
                                <?php if($value['stt_order'] == 0){
                                ?>
                                <td><a href="index.php?page=admin&method=list-order&id_order=<?=$value['id_order']?>" class="btn btn-primary">Xác nhận</a></td>
                                <?php
                                }else{
                                ?>
                                <td style="color:green;">đã xác nhận</td>
                                <?php
                                } ?>
                                <td><?php echo $value['date_order']; ?></td>
                                <td>
                                    <!-- <button class="btn btn-primary">Edit</button> -->
                                    <a href="index.php?page=admin&method=edit-order&id=<?php echo $value['id_order']; ?>" >
                                        <button class="btn btn-success">Sửa</button>
                                    </a>
                                    <a href="index.php?page=admin&method=del-order&id=<?php echo $value['id_order']; ?>" onclick="return confirm('Bạn có thực sự muốn xóa sản phẩm này không? ');">
                                        <button class="btn btn-danger">Xóa</button>
                                    </a>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-primary btn_detail_order" value="<?php echo $value['id_order']; ?>" data-toggle="modal" data-target=".detail-order">Chi tiết</button>
                                </td>
                            </tr>
                            <?php	
                                }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- end row -->
<!-- ---------------------------------------------------------------------- -->
<!-- show detail order -->
<div class="modal fade detail-order" id="Modal_order" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" style="width: 800px">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="padding: 40px 40px;">
        
      </div>
    </div>
  </div>
</div>
<!-- ---------------------------------------------------------------------- -->
<!-- <script>
    $(document),ready(function(){ 
        $('example').DataTable();
    });
</script> -->