<?php  
	if(isset($ajax_flag)){
		include_once '../../../config/myConfig.php';
	}else{
		include_once '../config/myConfig.php';
	}
	class Admin_m extends Connect
	{
		
		function __construct(){
			parent::__construct(); // Gọi hàm __construct bên myConfig, luôn tồn tại $pdo để kết nối tới CSDL
        }
        
        // User

		public function Login_m($email, $password){
			$sql = "SELECT * FROM tbl_user WHERE email = :email AND password = :password";
			$pre = $this->pdo->prepare($sql); 
			$pre->bindParam(":email", $email);
			$pre->bindParam(":password", $password);
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}

		// ---------------------------------------------------------------------------------------
		// Quản lý sản phẩm

		public function getProduct_m(){
			$sql = 'SELECT * FROM tbl_cate, tbl_product WHERE tbl_cate.id_cate = tbl_product.id_cate ORDER BY tbl_product.id DESC';
			$pre = $this->pdo->prepare($sql); // chuẩn bị thực thi câu lệnh truy vấn
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}

		public function getBrand_m(){
			$sql = 'SELECT * FROM tbl_brand';
			$pre = $this->pdo->prepare($sql); // chuẩn bị thực thi câu lệnh truy vấn
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}

		public function searchPhones_m($name){
			$sql = "SELECT * FROM cate,tbl_product WHERE cate.id_cate = tbl_product.id_cate AND name LIKE '%$name%'";
            $result =  $this->pdo->prepare($sql);
            $result->execute();
            return $result->fetchAll(PDO::FETCH_ASSOC);
		}
		
		public function delProduct_m($id){
			
			$sql = "DELETE FROM tbl_product WHERE id = :id";
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(':id', $id);
            return $pre->execute();
		}

		public function addProduct_m($name, $id_cate, $id_brand, $price, $img, $quantity, $description, $slug){
			$sql = "INSERT INTO tbl_product(name, id_cate, id_brand, price, img, quantity, description, slug) 
					VALUES(:name, :id_cate, :id_brand, :price, :img, :quantity, :description, :slug)";

			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(':name', $name);
			$pre->bindParam(':id_cate', $id_cate);
			$pre->bindParam(':id_brand', $id_brand);
			$pre->bindParam(':price', $price);
			$pre->bindParam(':img', $img);
			$pre->bindParam(':quantity', $quantity);
			$pre->bindParam(':description', $description);
			$pre->bindParam(':slug', $slug);
			$addProduct = $pre->execute();

			if ( $addProduct ){
				$_SESSION['noti'] = 1;
			}else{
				echo "Thêm mới thất bại";
			}
		}

        //convert name img 
        protected function convert_name($str) {
            $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
            $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
            $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
            $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
            $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
            $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
            $str = preg_replace("/(đ)/", 'd', $str);
            $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
            $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
            $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
            $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
            $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
            $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
            $str = preg_replace("/(Đ)/", 'D', $str);
            $str = preg_replace("/(\“|\”|\‘|\’|\,|\!|\&|\;|\@|\#|\%|\~|\`|\=|\_|\'|\]|\[|\}|\{|\)|\(|\+|\^)/", '-', $str);
            $str = preg_replace("/( )/", '-', $str);
            return $str;
        }

		// Lấy thông tin phone theo id
		public function getProductID_m($id){
			$sql = "SELECT *FROM tbl_product WHERE id = :id";
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id", $id);
			$pre->execute();
			return $pre->fetch(PDO::FETCH_ASSOC);
		}

		// Cập nhật thông tin sp
		public function editProduct_m($id, $name, $id_cate, $price, $img, $quantity, $description, $slug){
			$sql = "UPDATE tbl_product 
					SET name = :name, id_cate = :id_cate, price = :price, img = :img, quantity = :quantity, description = :description, slug = :slug 
					WHERE id = :id";

			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id", $id);
			$pre->bindParam(":name", $name);
			$pre->bindParam(":id_cate", $id_cate);
			$pre->bindParam(":price", $price);
			$pre->bindParam(":img", $img);
			$pre->bindParam(":quantity", $quantity);
			$pre->bindParam(":description", $description);
			$pre->bindParam(":slug", $slug);
			$update = $pre->execute();

			if ($update) {
				$_SESSION['noti'] = 2;
			}else{
				echo "Cập nhật thất bại!";
			}
		}

		// ---------------------------------------------------------------------------------------
		// Quản lý danh mục

		public function getCate_m(){
			$sql = 'SELECT * FROM tbl_cate';
			$pre = $this->pdo->prepare($sql); // chuẩn bị thực thi câu lệnh truy vấn
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}

		public function delCate_m($id){
			
			$sql = "DELETE FROM tbl_cate WHERE id_cate = :id";
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(':id', $id);
            return $pre->execute();
		}

		public function addCate_m($name_cate){
			$sql = "INSERT INTO tbl_cate(name_cate) 
					VALUES(:name_cate)";

			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(':name_cate', $name_cate);
			$addCate = $pre->execute();

			if ( $addCate ){
				$_SESSION['noti'] = 1;
			}else{
				echo "Thêm mới thất bại";
			}
		}

		public function getCateID_m($id){
			$sql = "SELECT *FROM tbl_cate WHERE id_cate = :id";
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id", $id);
			$pre->execute();
			return $pre->fetch(PDO::FETCH_ASSOC);
		}

		public function editCate_m($id, $name_cate){
			$sql = "UPDATE tbl_cate
					SET name_cate = :name_cate
					WHERE id_cate = :id";

			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id", $id);
			$pre->bindParam(':name_cate', $name_cate);
			$update = $pre->execute();

			if ($update) {
				$_SESSION['noti'] = 2;
			}else{
				echo "Cập nhật thất bại!";
			}
		}
		// ---------------------------------------------------------------------------------------
		// Quản lý khách hàng

		public function getMember_m(){
			$sql = 'SELECT * FROM tbl_member ORDER BY id_member DESC';
			$pre = $this->pdo->prepare($sql); // chuẩn bị thực thi câu lệnh truy vấn
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}

		public function delMember_m($id){
			
			$sql = "DELETE FROM tbl_member WHERE id_member = :id";
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(':id', $id);
            return $pre->execute();
		}

		public function searchCustomer_m($key){
			$sql = "SELECT * FROM customer WHERE name_customer LIKE '%$key%' OR phone_customer LIKE '%$key%'";
            $result =  $this->pdo->prepare($sql);
            $result->execute();
            return $result->fetchAll(PDO::FETCH_ASSOC);
		}

		public function addMember_m($name_member, $phone_member, $email_member, $addres, $password){
			$sql = "INSERT INTO tbl_member(name_member, phone_member, email_member, addres, password) 
					VALUES(:name_member, :phone_member, :email_member, :addres, :password)";

			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(':name_member', $name_member);
			$pre->bindParam(':phone_member', $phone_member);
			$pre->bindParam(':email_member', $email_member);
			$pre->bindParam(':addres', $addres);
			$pre->bindParam(':password', $password);
			$addMember = $pre->execute();

			if ( $addMember ){
				$_SESSION['noti'] = 1;
			}else{
				echo "Thêm mới thất bại";
			}
		}

		// Lấy thông tin khách hàng theo id
		public function getMemberID_m($id){
			$sql = "SELECT *FROM tbl_member WHERE id_member = :id";
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id", $id);
			$pre->execute();
			return $pre->fetch(PDO::FETCH_ASSOC);
		}
		
		// Cập nhật thông tin khách hàng
		public function editMember_m($id, $name_member, $phone_member, $email_member, $addres){
			$sql = "UPDATE tbl_member 
					SET name_member = :name_member, phone_member = :phone_member, email_member = :email_member, addres = :addres
					WHERE id_member = :id";

			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id", $id);
			$pre->bindParam(":name_member", $name_member);
			$pre->bindParam(":phone_member", $phone_member);
			$pre->bindParam(":email_member", $email_member);
			$pre->bindParam(":addres", $addres);
			$update = $pre->execute();

			if ($update) {
				$_SESSION['noti'] = 2;
			}else{
				echo "Cập nhật thất bại!";
			}
		}

		// ---------------------------------------------------------------------------------------
		// Quản lý đơn hàng
		
		public function getOrder_m(){
			$sql = 'SELECT tbl_order.*, tbl_member.*
			FROM tbl_order, tbl_member
			WHERE  tbl_order.id_member = tbl_member.id_member
			ORDER BY tbl_order.id_order DESC';
			$pre = $this->pdo->prepare($sql); // chuẩn bị thực thi câu lệnh truy vấn
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}

		public function searchOrder_m($key){
			$sql = "SELECT * FROM tbl_member,tbl_order
			WHERE tbl_member.id_member = tbl_order.id_member
			AND name_member LIKE '%$key%' OR phone_member LIKE '%$key%'
			ORDER BY tbl_order.id_order DESC ";
            $result =  $this->pdo->prepare($sql);
            $result->execute();
            return $result->fetchAll(PDO::FETCH_ASSOC);
		}

		//Hàm lấy ra thông tin chi tiết đơn hàng để xử lý ajax show_detail_order
		public function getOrderDetailID_m($id){
			$sql = 'SELECT tbl_order.*, tbl_detail_order.*, tbl_product.name
			FROM tbl_order  
			INNER JOIN tbl_detail_order ON tbl_order.id_order 		= tbl_detail_order.id_order 
			INNER JOIN tbl_product 		ON tbl_detail_order.id_product	= tbl_product.id 
			WHERE tbl_order.id_order = :id_order
			ORDER BY tbl_order.id_order DESC ';
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(':id_order', $id);
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}

		//Xoá 1 đơn hàng ở bảng order
		public function delOrder_m($id){
			$sql="DELETE FROM tbl_order WHERE id_order = :id_order";
			$pre=$this->pdo->prepare($sql);
			$pre->bindParam(':id_order',$id);
			return $pre->execute();       
		}

		//Xoá 1 đơn hàng ở bảng orderdetail
		public function delOrderDetail_m($id){
			$sql="DELETE FROM tbl_detail_order WHERE id_order = :id_order";
			$pre=$this->pdo->prepare($sql);
			$pre->bindParam(':id_order',$id);
			return $pre->execute();       
		}

		//Lấy ra thông tin đơn hàng dựa theo ID để xử lý ajax order
		public function getOrderID_m($id){
			$sqls = 'SELECT tbl_order.*, tbl_member.*, tbl_detail_order.total
			FROM customer, tbl_order, order_detail
			WHERE customer.customer_id = tbl_order.customer_id AND tbl_order.order_id = order_detail.order_id AND tbl_order.order_id = :order_id';
			$sql = 'SELECT  tbl_member.*, tbl_order.*, tbl_detail_order.*, tbl_product.name
			FROM tbl_member 
			INNER JOIN tbl_order 	ON tbl_member.id_member 	= tbl_order.id_member  
			INNER JOIN tbl_detail_order ON tbl_order.id_order 		= tbl_detail_order.id_order 
			INNER JOIN tbl_product 		ON tbl_detail_order.id_product 	= tbl_product.id 
			WHERE tbl_order.id_order = :id_order
			ORDER BY tbl_order.id_order DESC ';
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(':id_order', $id);
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}

		//Hàm lấy ra thông tin trạng thái đơn hàng
		public function getOrderStt_m(){
			$sql = 'SELECT * FROM tbl_order_stt ';
			$pre = $this->pdo->prepare($sql);
			$pre->execute();
			return $pre->fetchAll(PDO::FETCH_ASSOC);
		}

		// Cập nhật thông tin đơn hàng trong bảng order
		public function editOrder_m($id, $note, $stt_order){
			$sql = "UPDATE tbl_order 
					SET note = :note, stt_order = :stt_order
					WHERE id_order = :id_order";

			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id_order", $id);
			$pre->bindParam(":note", $note);
			$pre->bindParam(":stt_order", $stt_order);
			$update = $pre->execute();

			if ($update) {
				$_SESSION['noti'] = 2;
			}else{
				echo "Cập nhật thất bại!";
			}
		}

		// Cập nhật thông tin đơn hàng trong bảng customer
		public function editOrder_Customer_m($id, $stt_order){
			$sql = "UPDATE tbl_order 
					SET stt_order = :stt_order
					WHERE id_order = :id_order";

			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(":id_order", $id);
			$pre->bindParam(":note", $note);
			$pre->bindParam(":stt_order", $stt_order);
			$update = $pre->execute();

			if ($update) {
				$_SESSION['noti'] = 2;
			}else{
				echo "Cập nhật thất bại!";
			}
		}

		// xác nhận 1 đơn hàng
        protected function confirmOrder_m($id)
        {
            $sql = "UPDATE `tbl_order` SET `stt_order`= 1 WHERE `id_order` = :id";
            $pre = $this->pdo->prepare($sql);
            $pre->bindParam(":id", $id);
            $pre->execute();
        }
		// ---------------------------------------------------------------------------------------
        // add program
        protected function addProgram_m($title, $rose, $description,$category)
        {
            $sql = "INSERT INTO `tbl_program`(`title`, `category` , `rose`, `description`) VALUES (:title,:category, :rose, :description)" ;
            $pre = $this->pdo->prepare($sql);
			$pre->bindParam(":title", $title);
            $pre->bindParam(":rose", $rose);
            $pre->bindParam(":category", $category);
			$pre->bindParam(":description", $description);
		    $pre->execute();
        }

        // select program
        protected function selectProgram()
        {
            $sql = "SELECT * FROM tbl_program";
            $pre = $this->pdo->prepare($sql);
            $pre->execute();
            $resutl = $pre->fetchAll(PDO::FETCH_ASSOC);
			return $resutl;
        }
        
        // select referal
        protected function listAffiliatePro_m()
        {
            $sql = "SELECT tbl_referal.id as 'referal_id' , tbl_affiliate.name , tbl_program.title , tbl_program.rose, tbl_program.category , tbl_referal.status , tbl_referal.created_at FROM tbl_referal , tbl_program, tbl_affiliate WHERE tbl_referal.affiliate_id = tbl_affiliate.affiliate_id AND tbl_referal.program_id = tbl_program.program_id";
            $pre = $this->pdo->prepare($sql);
            $pre->execute();
            $resutl = $pre->fetchAll(PDO::FETCH_ASSOC);
			return $resutl;
        }

        // phe duyệt affiliate
        protected function approved_m($id)
        {
            $sql = "UPDATE `tbl_referal` SET `status`= 1 WHERE id = :id";
            $pre = $this->pdo->prepare($sql);
            $pre->bindParam(":id", $id);
            $pre->execute();
        }
        // stop affiliate 
        protected function stop_m($id)
        {
            $sql = "UPDATE `tbl_referal` SET `status`= 0 WHERE id = :id";
            $pre = $this->pdo->prepare($sql);
            $pre->bindParam(":id", $id);
            $pre->execute();
        }

        // xóa referal
        protected function delReferal_m($id)
        {
            $sql = "DELETE FROM `tbl_referal` WHERE id = :id";
            $pre = $this->pdo->prepare($sql);
            $pre->bindParam(":id", $id);
            $pre->execute();
        }

        // select doanh số affiliate
        protected function listSaleAffiliate_m()
        {
            $sql = "SELECT tbl_member.name_member as 'nameMember' , tbl_product.name as 'nameProduct' , tbl_detail_order.price , tbl_detail_order.quantity , tbl_detail_order.total , tbl_affiliate.name as 'nameAffiliate' , tbl_program.title as 'nameProgram' , tbl_program.rose, tbl_program.category  , tbl_order_referal.status , tbl_order_referal.id as 'orderReferal_id'
            FROM tbl_member , tbl_product , tbl_detail_order , tbl_affiliate , tbl_program , tbl_order_referal , tbl_order , tbl_referal
            WHERE tbl_order.id_member = tbl_member.id_member 
            AND tbl_detail_order.id_product = tbl_product.id 
            AND tbl_order_referal.order_id = tbl_order.id_order
            AND tbl_order_referal.order_id = tbl_detail_order.id_order
            AND tbl_order_referal.referal_id = tbl_referal.id
            AND tbl_referal.affiliate_id = tbl_affiliate.affiliate_id
            AND tbl_referal.program_id = tbl_program.program_id";
            $pre = $this->pdo->prepare($sql);
            $pre->execute();
            $resutl = $pre->fetchAll(PDO::FETCH_ASSOC);
			return $resutl;
        }
        

        // xác nhận tất cả doanh số affiliate
        protected function confirmAll_m()
        {
            $sql = "UPDATE `tbl_order_referal` SET `status`= 1";
            $pre = $this->pdo->prepare($sql);
            $pre->execute();
        }

        // xác nhận 1 doanh số affiliate
        protected function confirmDetail_m($id)
        {
            $sql = "UPDATE `tbl_order_referal` SET `status`= 1 WHERE `order_id` = :id";
            $pre = $this->pdo->prepare($sql);
            $pre->bindParam(":id", $id);
            $pre->execute();
        }

    }
?>
