<?php
    include_once "../../Model/productModel.php";

    class Product_c extends Product_m
{
    private $mem;

    function __construct()
    {
        $this->mem = new Product_m(); // Tự động chạy cái hàm __construct
    }

    public function getProductId_c($id){
        $product = $this->mem->getProductId($id);
        return $product;
    }
}
?>