<?php  
	include_once "Model/affiliate_m.php";
	/**
	 * 
	 */
	class Affiliate_c extends Affiliate_m
	{
		private $affiliate;

		function __construct()
		{
			$this->affiliate = new Affiliate_m(); // Tự động chạy cái hàm __construct
		}
        
        public function listAffiliate_c()
        {
            $listAffiliate = $this->affiliate->listAffiliate_m();
            include_once 'Views/page/affiliate/list_affiliate.php';
        }

        public function login_c()
        {
            include_once 'Views/page/affiliate/login.php';
        }

        public function register_c()
        {
            include_once 'Views/page/affiliate/register.php';
        }

        //danh sách chương trình đối tác đã đăng ký
        public function listAffMember()
        {
            if(isset($_GET['affiliate_id'])){
                $affiliate_id = $_GET['affiliate_id'];
                $listAffMember = $this->affiliate->listAffMember_m($affiliate_id);
            }
            include_once 'Views/page/affiliate/affiliate_member/list_aff_member.php'; 
        }

        // chi tiết doanh số bán hàng affiliate
        public function listSaleAffDetail_c()
        {
            if(isset($_GET['affiliate_id'])){
                $affiliate_id = $_GET['affiliate_id'];
                $listSaleAffDetail = $this->affiliate->listSaleAffDetail_m($affiliate_id);
            }
            include_once 'Views/page/affiliate/affiliate_member/list_sale_detail.php'; 
        }

        public function option()
        {
            if(isset($_GET['method'])){
                $method = $_GET['method'];
            }else{
                $method = "listAffiliate";
            }

            switch ($method) {
                case 'listAffiliate':
                    $this->listAffiliate_c();
                    break;
                case 'login' : 
                    $this->login_c();
                    if(isset($_POST['login_aff'])){
                        $email = $_POST['email'];
                        $password = $_POST['password'];
                        if(!empty($email) && !empty($password)){
                            $check = $this->affiliate->login_m($email,$password);
                            if(!empty($check)){
                                $_SESSION['aff_member_id'] = $check[0]['affiliate_id'];
                                $_SESSION['check'] = 1;
                                header("Location:index.php?page=affiliate&method=listAffiliate");
                            }else{
                                $_SESSION['check'] = 2;
                                header("Location:index.php?page=affiliate&method=login");
                            }
                        }
                        
                    }
                    break;
                case 'logout':
                    unset($_SESSION['aff_member_id']) ;
                    header('Location:index.php?page=affiliate&method=listAffiliate');
                    break ;
                case 'register' : 
                    $this->register_c();
                    if(isset($_POST['register_aff'])){
                        $name = $_POST['name'];
                        $email = $_POST['email'];
                        $phone = $_POST['phone'];
                        $password = $_POST['password'];
                        $address= $_POST['address'];
                        $check = $this->affiliate->checkAccAffiliate($email);
                        if(!empty($name) && !empty($email) && !empty($phone) && !empty($password) && !empty($address)){
                            if(empty($check)){
                                $this->affiliate->register_m($name, $email, $phone, $password, $address);
                                $_SESSION['check'] = 1;
                                header("Location:index.php?page=affiliate&method=login");
                            }else{
                                $_SESSION['check'] = 2;
                                header("Location:index.php?page=affiliate&method=register");
                            }
                            
                        }   
                    }
                    break;
                case 'list_aff_member':
                    $this->listAffMember();
                    if(isset($_GET['stop'])){
                        $id= $_GET['stop'];
                        $this->affiliate->delReferal_m($id);
                        header("Location:index.php?page=affiliate&method=list_aff_member&affiliate_id={$_GET['affiliate_id']}");
                    }
                    break;
                case 'addAffiliate' : 
                    if(isset($_GET['id_pro']) && !empty($_GET['id_pro']) && isset($_GET['id_aff']) && !empty($_GET['id_aff'])){
                        $affiliate_id = $_GET['id_aff'];
                        $program_id = $_GET['id_pro'];
                        $rand = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        $affiliate_code = $program_id.".".$affiliate_id.".".substr(str_shuffle($rand), 0, 16) ;
                        $checkReferal = $this->affiliate->checkReferal_m($affiliate_id, $program_id);
                        if(empty($checkReferal)){
                            $this->affiliate->addAffiliate_m($affiliate_id, $program_id, $affiliate_code);
                        }else{
                            $_SESSION['check'] = 4;
                        }
                        header("Location:index.php?page=affiliate&method=list_aff_member&affiliate_id=$affiliate_id"); 
                    }else{
                        $_SESSION['check'] = 3;
                        header("Location:index.php?page=affiliate&method=login");
                    }
                    break ;
                case 'listSaleAffDetail' : 
                    $this->listSaleAffDetail_c();
                    break ;
                default:
                header('Location:404.html');
                    break;
            }
        }
    }
?>
