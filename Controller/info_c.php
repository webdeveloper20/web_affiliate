<?php  
	include_once "Model/info_m.php";
	/**
	 * 
	 */
	class Info_c extends Info_m
	{
		private $mem;

		function __construct()
		{
			$this->mem = new Info_m(); // Tự động chạy cái hàm __construct
        }
        
        public function contactController(){
            include_once "Views/page/contact/contact-us.php";
        }
		
		public function Info_c(){
			if (isset($_GET['method'])) {
				$method = $_GET['method'];
			}else{ 
				$method = 'login';
			}

			switch ($method) {
				case 'login':
					include_once "Views/page/info/login.php";
                    break;
                case 'contact-us':
                    $this->contactController();
                    break;

                default:
                header('Location:404.html');
				break;
			}
		}
    }
?>