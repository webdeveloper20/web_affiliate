<?php
include_once "Model/product_m.php";
include_once 'PHPMailer/class.phpmailer.php';
include_once 'PHPMailer/class.smtp.php';
/**
 * 
 */
class Product_c extends Product_m
{
    private $mem;

    function __construct()
    {
        $this->mem = new Product_m(); // Tự động chạy cái hàm __construct
    }

    public function Product_c()
    {
        if (isset($_GET['method'])) {
            $method = $_GET['method'];
        } else {
            $method = 'home';
        }

        switch ($method) {
            case 'home':
                $trendProduct = $this->mem->trendProduct_m();
                $cate = $this->mem->getCate_m();
                $brand = $this->mem->getBrand_m();
                
                include_once "Views/page/product/home.php";
                break;

            case 'cart':
                if (isset($_GET['id'])) {
                    $id = (int)$_GET['id'];

                    $product = $this->mem->getProductId($id);
                    // echo "<pre>";
                    // print_r($product);
                    // echo "</pre>";
                    // Khởi tạo biến $_SESSION['cart'] lưu thông tin sản phẩm khách mua			
                    if (!isset($_SESSION['cart']) || empty($_SESSION['cart'])) {
                        // nếu giỏ hàng ko tồn tại session cart hoặc giỏ hàng trống
                        $_SESSION['cart'][$id] = $product;
                        $_SESSION['cart'][$id]['qty'] = 1;
                    } else {
                        if (array_key_exists($id, $_SESSION['cart'])) {
                            $_SESSION['cart'][$id]['qty'] += 1;
                        } else {
                            $_SESSION['cart'][$id] = $product;
                            $_SESSION['cart'][$id]['qty'] = 1;
                        }
                    }
                    $_SESSION['notiCart'] = 1;
                    // header("Location: index.php?page=home&method=shop");
                    header("Location: index.php?page=home");
                    // echo "<pre>";
                    // print_r($_SESSION['cart']);
                    // echo "</pre>";
                }
                include_once "Views/page/product/cart.php";
                break;

            case 'delCart':
                if (isset($_GET['id'])) {
                    $id = (int)$_GET['id'];
                    if (isset($_SESSION['cart']) && !empty($_SESSION['cart'])) {
                        unset($_SESSION['cart'][$id]);
                    }
                }
                include_once "Views/page/product/cart.php";
                break;
            case 'checkout':
                if (isset($_POST['ordered'])) {
                    $name   = $_POST['name'];
                    $phone  = $_POST['phone'];
                    $email  = $_POST['email'];
                    $addres = $_POST['addres'];
                    $note   = $_POST['note'];

                    $password = md5('shopping.com');
                    // echo $name.$phone.$email.$addres.$note;
                    // die();

                    // ADD thông tin người mua hàng
                    $addMember = $this->mem->addMember_m($name, $phone, $email, $addres, $password);
                    $id_member = $this->mem->pdo->lastInsertId();

                    // echo $customer_id ;
                    // die();

                    $order = $this->mem->addOrder_m($id_member, $note);
                    $id_order = $this->mem->pdo->lastInsertId();

                    // echo $order_id ;
                    // die();

                    foreach ($_SESSION['cart'] as $pro) {
                        $total = $pro['qty'] * $pro['price'];

                        // ADD detail_order
                        $this->mem->addOrderDetail_m($id_order, $pro['id'], $pro['qty'], $pro['price'], $total);
                    }
                    //check affiliate và thêm vào bảng order_referal
                    if (isset($_COOKIE['program_id']) && !empty($_COOKIE['program_id']) && isset($_COOKIE['affiliate_id']) && !empty($_COOKIE['affiliate_id']) && isset($_COOKIE['affiliate_code']) && !empty($_COOKIE['affiliate_code'])) {

                        $checkProgram = $this->mem->checkProgram($_COOKIE['program_id']);
                        $checkAffiliate = $this->mem->checkAffiliate($_COOKIE['affiliate_id']);
                        $checkReferal = $this->mem->checkReferal($_COOKIE['affiliate_id'], $_COOKIE['program_id'], $_COOKIE['affiliate_code']);
    
                        if (!empty($checkProgram) && !empty($checkAffiliate) && !empty($checkReferal)) {
                            $referal_id =  $checkReferal['id'];
                            $this->mem->addOrderReferal($id_order, $referal_id);

                             // // Instantiation and passing `true` enables exceptions	
                             // gửi email cho affiliate
                            $mail = new PHPMailer(true);
                            try {

                                $mail ->CharSet = "UTF-8";

                                //Server settings
                                // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
                                $mail->SMTPDebug = 2;   // Sau này đưa lên localhost sẽ là 0             // Enable verbose debug output
                                $mail->isSMTP();                                            // Send using SMTP
                                $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                                $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                                $mail->Username   = 'shopquanaoonline20@gmail.com';                     // SMTP username
                                $mail->Password   = 'QuyenA20';                               // SMTP password
                                $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

                                //Recipients
                                $mail->setFrom("shopquanaoonline20@gmail.com", 'Fashion shop');
                                $mail->addAddress($checkAffiliate[0]['email'],$checkAffiliate[0]['name']);     // Add a recipient

                                // // Attachments
                                // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                                // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

                                // Content
                                $mail->isHTML(true);                                  // Set email format to HTML
                                $mail->Subject = 'Fashion shop thông báo';
                                $mail->Body    = 'Chúc mừng bạn đã có khách hàng mua hàng qua link bán hàng của bạn hãy nhanh chóng kiểm tra ngay nào';
                                // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                                $mail->send();
                                echo 'Message has been sent';
                            } catch (Exception $e) {
                                echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                            }
                        }
                    }
                    
                    // include_once 'PHPMailer/class.phpmailer.php';
                    // include_once 'PHPMailer/class.smtp.php';

                    // // Instantiation and passing `true` enables exceptions	
                    // $mail = new PHPMailer(true);

                    // try {

                    // 	$mail ->CharSet = "UTF-8";

                    // 	//Server settings
                    // 	// $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
                    // 	$mail->SMTPDebug = 2;   // Sau này đưa lên localhost sẽ là 0             // Enable verbose debug output
                    // 	$mail->isSMTP();                                            // Send using SMTP
                    // 	$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                    // 	$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                    // 	$mail->Username   = 'ducoz1999@gmail.com';                     // SMTP username
                    // 	$mail->Password   = '0962790606';                               // SMTP password
                    // 	$mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

                    // 	//Recipients
                    // 	$mail->setFrom('hotro@fashion.com', 'Đơn hàng mới');
                    // 	$mail->addAddress($email, $name);     // Add a recipient

                    // 	// // Attachments
                    // 	// $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                    // 	// $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

                    // 	// Content
                    // 	$mail->isHTML(true);                                  // Set email format to HTML
                    // 	$mail->Subject = 'Đơn hàng mới tại Fashion.com';
                    // 	$mail->Body    = 'Nội dung mail được gửi đi';
                    // 	// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                    // 	$mail->send();
                    // 	echo 'Message has been sent';
                    // } catch (Exception $e) {
                    // 	echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                    // }

                    $_SESSION['notiCart'] = 2;
                    unset($_SESSION['cart']);
                    // header("Location: index.php?page=home&method=shop");
                    header("Location: index.php?page=home");
                }
                include_once "Views/page/product/checkout.php";
                break;

            case 'product':
                if (isset($_GET['id'])){
                    $id = (int)$_GET['id'];
                    $product = $this->mem->getProductDetailId_m($id);
                    // $imgPhone = $this->mem->imgProduct_m($id);

                    // echo "<pre>";
                    // print_r($product);
                    // echo "</pre>";
                }
                $getCurURL = $this->mem->getCurURL();
                include_once "Views/page/product/product_detail.php";
                break;

            case 'shop':
                if ( isset($_GET['pages'])){
                    $pages = (int)$_GET['pages'];
                }else{
                    $pages = 1;
                    $_GET['pages'] = 1;
                }

                $record = 6;
                $listProduct = $this->mem->getProduct_m($pages, $record);
                $totalRecord = $this->mem->countMember_m();
                // totalRecord : tổng số bản ghi
                // record : số bản ghi trên 1 trang
                $dataPage = ceil($totalRecord / $record); // ceil làm tròn

				$cate = $this->mem->getCate_m();
				$brand = $this->mem->getBrand_m();

                if (isset($_GET['id_cate'])){
                    $id = (int)$_GET['id_cate'];

                    // Hiện thị theo danh mục
                    $listProduct = $this->mem->getCateShop_m($id);
                }
                if (isset($_GET['id_brand'])){
                    $id = (int)$_GET['id_brand'];

                    // Hiện thị theo danh mục
                    $listProduct = $this->mem->getBrandShop_m($id);
                }
                include_once "Views/page/product/shop.php";
                break;
            case 'product_like':
                include_once "Views/page/product/product_like.php";
                break;
            default:
            header('Location:404.html');
               break;
        }
    }
}
?>
