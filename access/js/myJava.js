$(document).ready(function(){
	$(document).on('click', '.del-cart', function(e){
		e.preventDefault();
		var id = $(this).val();
		
		var check = confirm('Bạn có muốn xóa sản phẩm khỏi giỏ hàng không?');

		if (check) {
			$.ajax({
				url 		: 'server/del_cart.php',
				type 		: 'POST',
				dataType 	: 'html',
				data 		: { id : id },

				success : function(data){
					console.log(data);
					// $("#table-cart").load(" .data-table");
					$("#table-cart").load(" #data-table");
				}
			});
		}
	});

	$(document).on('change', '.update-cart', function(e){
		var qty = $(this).val();
		var id = $(this).attr('product_id');
		// alert('id = ' + id + " qty = " + qty);
		// die();
		// Xử lý số âm và ký tự nhập vào?

		$.ajax({
			url 		: 'server/cart/update_cart.php',
			type 		: 'POST',
			dataType 	: 'html',
			data 		: { id : id, qty : qty },

			success : function(data){
				$("#table-cart").load(" #data-table");
				// $("#total-order").load(" .subtotal");
				$("#total-order").load(" #subtotal");
			}
		});
	});

});

// Thêm sản phẩm vào giỏ hàng
$(document).on('click', '#add-cartss', function(e){
	e.preventDefault();
    var id = $(this).attr('product_id');
    $.ajax({
        url 		: 'server/cart/add_cart.php',
        type 		: 'POST',
        dataType 	: 'html',
        data 		: { id : id },
        success : function(data){
           $('.noti-cart').html(data);
           var audio = new Audio('access/audio/nhacchuong.mp3');
           audio.play();   
           $('.animate__animated').delay(5000).slideUp();
        }
    });
});

// thêm vào sản phẩm yêu thích
$(document).on('click', '#add-like', function(e){
	e.preventDefault();
    var id = $(this).attr('product_id');
    $.ajax({
        url 		: 'server/product/add_like.php',
        type 		: 'POST',
        dataType 	: 'html',
        data 		: { id : id },
        success : function(data){
           $('.noti-cart').html(data);
           var audio = new Audio('access/audio/nhacchuong.mp3');
           audio.play();   
            $("#like-product-"+id).load(" #"+id);
            // $("#like-product-"+id).load(" #like");
            // $("#table-cart").load(" #data-table");
        }
    });
});

// xóa khỏi sản phẩm yêu thích
$(document).on('click', '#dislike', function(e){
	e.preventDefault();
    var id = $(this).attr('product_id');
    $.ajax({
        url 		: 'server/product/dislike.php',
        type 		: 'POST',
        dataType 	: 'html',
        data 		: { id : id },
        success : function(data){
           $('.noti-cart').html(data);
           var audio = new Audio('access/audio/nhacchuong2.mp3');
           audio.play();   
           $('.animate__animated').delay(5000).slideUp();
            $("#table-cart").load(" #data-table");
        }
    });
});