// validation register acc affiliate
function FormValidate(){
    var name = document.getElementById('name').value;
    var errorName = document.getElementById('errorName');
    var regexName = /^[^\d+]*[\d+]{0}[^\d+]*$/;

    var address = document.getElementById('address').value;
    var errorAddress = document.getElementById('errorAddress');
    var regexAddress = /^[^\d+]*[\d+]{0}[^\d+]*$/;

    var phone = document.getElementById('phone').value;
    var erorPhone = document.getElementById('erorPhone');
    var regexPhone = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

    var email = document.getElementById('email').value;
    var errorEmail = document.getElementById('erorEmail');
    var reGexEmail = /[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}/igm;
    
    if (name == '' || name == null) {
        errorName.innerHTML = "Họ tên không được để trống!";
    }else if(!regexName.test(name)){
        errorName.innerHTML = "Họ tên không hợp lệ!";
    }else{
        errorName.innerHTML = '';
    }

    if (address == '' || address == null) {
        errorAddress.innerHTML = "Địa chỉ không được để trống!";
    }else if(!regexAddress.test(address)){
        errorAddress.innerHTML = "Địa chỉ không hợp lệ!";
    }else{
        errorAddress.innerHTML = '';
    }

    if (phone == '' || phone == null) {
        erorPhone.innerHTML = "Số điện thoại không được để trống!";
    }else if(!regexPhone.test(phone)){
        erorPhone.innerHTML = "SĐT không hợp lệ!";
    }else{
        erorPhone.innerHTML = '';
    }

    if (email == '' || email == null) {
        errorEmail.innerHTML = "Email không được để trống!";
    }else if(!reGexEmail.test(email)){
        errorEmail.innerHTML = "Email không hợp lệ!";
        email = '';
    }else{
        errorEmail.innerHTML = '';
    }

    var passW = document.getElementById('pass').value;
    var errorPass = document.getElementById('errorPass');

    if (passW == '' || passW == null) {
        errorPass.innerHTML = "Mật khẩu vui lòng không để trống!";
    }else{
        errorPass.innerHTML = "";
    }


    if (name && phone && email && address && passW) {
        return true;
    }
    return false;
}
// validation checkout
function validationCheckout(){
    var name = document.getElementById('name').value;
    var errorName = document.getElementById('errorName');
    var regexName = /^[^\d+]*[\d+]{0}[^\d+]*$/;

    var address = document.getElementById('address').value;
    var errorAddress = document.getElementById('errorAddress');
    var regexAddress = /^[^\d+]*[\d+]{0}[^\d+]*$/;

    var phone = document.getElementById('phone').value;
    var erorPhone = document.getElementById('erorPhone');
    var regexPhone = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

    var email = document.getElementById('email').value;
    var errorEmail = document.getElementById('erorEmail');
    var reGexEmail = /[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}/igm;
    
    if (name == '' || name == null) {
        errorName.innerHTML = "Họ tên không được để trống!";
    }else if(!regexName.test(name)){
        errorName.innerHTML = "Họ tên không hợp lệ!";
    }else{
        errorName.innerHTML = '';
    }

    if (address == '' || address == null) {
        errorAddress.innerHTML = "Địa chỉ không được để trống!";
    }else if(!regexAddress.test(address)){
        errorAddress.innerHTML = "Địa chỉ không hợp lệ!";
    }else{
        errorAddress.innerHTML = '';
    }

    if (phone == '' || phone == null) {
        erorPhone.innerHTML = "Số điện thoại không được để trống!";
    }else if(!regexPhone.test(phone)){
        erorPhone.innerHTML = "SĐT không hợp lệ!";
    }else{
        erorPhone.innerHTML = '';
    }

    if (email == '' || email == null) {
        errorEmail.innerHTML = "Email không được để trống!";
    }else if(!reGexEmail.test(email)){
        errorEmail.innerHTML = "Email không hợp lệ!";
        email = '';
    }else{
        errorEmail.innerHTML = '';
    }

    if (name && phone && email && address) {
        return true;
    }
    return false;
}

$('.checkAccAffiliate').delay(2000).slideUp();