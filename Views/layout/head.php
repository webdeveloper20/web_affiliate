<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <!-- Favicons -->
    <link rel="shortcut icon" href="https://storage.googleapis.com/cdn.nhanh.vn/store/10103/store_1596768680_581.png">
    <!-- <link href="access/css/mdb.min.css" rel="stylesheet"> -->
    <link href="access/css/bootstrap.min.css" rel="stylesheet">
    <link href="access/css/font-awesome.min.css" rel="stylesheet">
    <link href="access/css/prettyPhoto.css" rel="stylesheet">
    <link href="access/css/price-range.css" rel="stylesheet">
    <link href="access/css/animate.css" rel="stylesheet">
	<link href="access/css/main.css" rel="stylesheet">
	<link href="access/css/responsive.css" rel="stylesheet">
    <link href="access/css/mycss.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="access/js/html5shiv.js"></script>
    <script src="access/js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="access/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="access/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="access/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="access/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="access/images/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />