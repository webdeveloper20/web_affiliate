<?php
if (isset($_SESSION['cart']) && !empty($_SESSION['cart'])) {

    // echo "<pre>";
    // print_r($_SESSION['cart']);
    // echo "</pre>";
?>
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb" style="margin-bottom: 10px;">
                    <li><a href="#">Home</a></li>
                    <li class="active">Check out</li>
                </ol>
            </div>
            <!--/breadcrums-->

            <div class="shopper-informations" style="padding-bottom: 100px;">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="contact-form">
                            <h2 class="title text-center">Thông tin khách hàng</h2>
                            <!-- <div class="status alert alert-success" style="display: none"></div> -->
                            <!-- <form action="" id="main-contact-form" class="contact-form row" name="contact-form" method="POST">
                                <div class="form-group col-md-12">
                                    <label>Tên <span class="required">*</span></label>
                                    <input type="text" name="name" class="form-control" required="required" placeholder="Tên">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Email <span class="required">*</span></label>
                                    <input type="email" name="email" class="form-control" required="required" placeholder="Email">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Điện thoại <span class="required">*</span></label>
                                    <input type="number" name="phone" class="form-control" required="required" placeholder="Số điện thoại">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Địa chỉ <span class="required">*</span></label>
                                    <input type="text" name="addres" class="form-control" required="required" placeholder="Địa chỉ">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Ghi chú </label>
                                    <textarea id="checkout-mess" name="note" cols="20" rows="10" placeholder="Bạn có thể thay đổi địa chỉ nhận hàng tại đây"></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <button type="submit" name="ordered" id="" class="btn btn-success">Đặt hàng</button>
                                </div>
                            </form> -->

                            <form action="" method="POST" name="checkout" onsubmit="return validationCheckout();">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="name">Họ tên : <span style="color: red;" id="errorName">(*)</span></label>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Tên" />
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="email">Email :<span style="color: red;" id="erorEmail">(*)</span></label>
                                        <input type="text" name="email" class="form-control" id="email" placeholder="Email" />
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="phone">Số điện thoại : <span style="color: red;" id="erorPhone">(*)</span></label>
                                        <input type="text" name="phone" class="form-control" id="phone" placeholder="Số điện thoại"/>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="address">Địa chỉ :<span style="color: red;" id="errorAddress">(*)</span></label>
                                        <input type="text" name="addres" class="form-control" id="address" placeholder="Địa chỉ" />
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="note">Ghi chú :</label>
                                        <textarea id="note" name="note" cols="20" rows="10" placeholder="Bạn có thể thay đổi địa chỉ nhận hàng tại đây"></textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <button type="submit" name="ordered" id="ordered" class="btn btn-success">Đặt hàng</button>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <div class="col-md-12">
                                <a class="btn btn-primary" href="index.php?page=home&method=cart">Trở lại giỏ hàng của bạn</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="register-req" style="padding: 14px 32px;">
                            <h3>Đơn hàng</h3>
                            <div>
                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <th class="description">Sản phẩm</th>
                                            <th class="total">Thành tiền</th>
                                        </tr>
                                    </thead>
                                    <?php
                                    $_SESSION['sumPrice'] = 0;
                                    foreach ($_SESSION['cart'] as $cart) {
                                    ?>
                                        <tbody>
                                            <tr class="">
                                                <td class="cart_product">
                                                    <?php echo $cart['name']; ?>
                                                    <span class="product-quantity"> × <?php echo $cart['qty']; ?></span>
                                                </td>
                                                <td class="cart_total">
                                                    <span class="amount">
                                                        <?php
                                                        $total = ($cart['price'] * $cart['qty']);
                                                        $_SESSION['sumPrice'] += $total;
                                                        echo number_format($total);
                                                        ?>
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    <?php
                                    }
                                    ?>
                                    <tfoot>
                                        <tr class="order-total">
                                            <th class="total" style=" font-weight: bold; font-size: 20px;">Tổng cộng</th>
                                            <td style="color: red; font-weight: bold; font-size: 20px;">
                                                <span class=" total amount"><?php echo number_format($_SESSION['sumPrice']); ?></span>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#cart_items-->
<?php
} else {
?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Thông báo: </strong> Giỏ hàng trống: <span> </span>
        <a href="index.php?page=home">Mua hàng</a>
    </div>
<?php
}
?>