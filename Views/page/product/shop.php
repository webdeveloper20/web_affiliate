<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Danh mục</h2>
                    <div class="panel-group category-products" id="accordian">
                        <!--category-productsr-->
                        <?php  
                            foreach ($cate as $val) { 
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="index.php?page=home&method=shop&id_cate=<?php echo $val['id_cate']; ?>"><?php echo $val['name_cate']; ?></a></h4>
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                    </div>
                    <!--/category-productsr-->

                    <div class="brands_products">
                        <!--brands_products-->
                        <h2>Thương hiệu</h2>
                        <div class="brands-name">
                            <ul class="nav nav-pills nav-stacked">
                            <?php  
                                foreach ($brand as $val) { 
                            ?>
                                <li><a href="index.php?page=home&method=shop&id_brand=<?php echo $val['id_brand']; ?>"> <span class="pull-right"></span><?php echo $val['brand_name']; ?></a></li>
                            <?php
                                }
                            ?>
                            </ul>
                        </div>
                    </div>
                    <!--/brands_products-->

                    <div class="price-range">
                        <!--price-range-->
                        <h2>Price Range</h2>
                        <div class="well">
                            <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2"><br />
                            <b>$ 0</b> <b class="pull-right">$ 600</b>
                        </div>
                    </div>
                    <!--/price-range-->
                </div>
            </div>

            <div class="col-sm-9 padding-right">
                <div class="features_items">
                    <!--features_items-->
                    <h2 class="title text-center">Danh sách sản phẩm</h2>
                    <?php
                    foreach ($listProduct as $lP) {
                    ?>
                    <div class="col-sm-4">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <a href="index.php?page=home&method=product&id=<?php echo $lP['id']; ?>">
                                        <img class="img-product" src="access/images/product/<?php echo $lP['img']; ?>" alt="<?php echo $lP['name']; ?>" style="width: 100%; height:340px" />
                                    </a>
                                    <h2><?php echo number_format($lP['price'])."đ"; ?></h2>
                                    <p><?php echo $lP['name']; ?></p>
                                    <div id="add-cartss" product_id="<?php echo $lP['id']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</div>
                                </div>
                                <!-- <div class="product-overlay">
                                    <div class="overlay-content">
                                        <h2>$56</h2>
                                        <p>Easy Polo Black Edition</p>
                                        <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <div>
                    <ul class="pagination">
                        <?php
                        if (isset($_GET['pages']) && $_GET['pages'] > 1) {
                            $back = $_GET['pages'] - 1;
                        ?>
                            <li><a href="index.php?page=home&method=shop&pages=<?php echo $back; ?>">&laquo;</a></li>
                        <?php
                        }
                        ?>
                        <?php
                        // echo $dataPage;
                        if (isset($dataPage) && $dataPage > 0) {
                            for ( $i = 1; $i <= $dataPage; $i++){
                        ?>
                            <li <?php if(isset($_GET['pages']) && $i == $_GET['pages']){echo 'class="active"';} ?>>
                                <a href="index.php?page=home&method=shop&pages=<?php echo $i; ?>"><?php echo $i; ?></a>
                            </li>
                        <?php
                            }
                        }
                        ?>
                        <?php
                        if (isset($_GET['pages']) && $_GET['pages'] < $dataPage) {
                            $next = $_GET['pages'] + 1;
                        ?>
                            <li><a href="index.php?page=home&method=shop&pages=<?php echo $next; ?>">&raquo;</a></li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
                <?php
                ?>
                <!--features_items-->
            </div>
        </div>
    </div>
</section>