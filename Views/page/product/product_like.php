<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <h2 class="title text-center">Danh sách yêu thích</h2>
        </div>
        <form action="" method="POST" name="frm_updateCart">
            <div class="table-responsive cart_info" id="table-cart">
                <table class="table table-condensed" id="data-table" style="text-align: center;">
                    <thead>
                        <tr class="cart_menu">
                            <td >Sản phẩm</td>
                            <td >Tên sản phẩm</td>
                            <td >Đơn giá</td>
                            <td colspan="2">Lựa chọn</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($_SESSION['like']) && !empty($_SESSION['like'])) {
                            foreach ($_SESSION['like'] as $like) {
                        ?>
                                <tr>
                                    <td class="cart_product">
                                        <a href=""><img style="width:110px; height:110px" src="access/images/product/<?php echo $like['img']; ?>" alt=""></a>
                                    </td>
                                    <td class="cart_description">
                                        <h4><a href=""><?php echo $like['name']; ?></a></h4>
                                    </td>
                                    <td class="cart_price">
                                        <p><?php echo number_format($like['price']); ?> đ</p>
                                    </td>
                                    <td class="cart_total">
                                        <div id="add-cartss" product_id="<?php echo $like['id']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</div>
                                    </td>
                                    <td class="cart_delete">
                                        <a class="cart_quantity_delete">
                                            <i class="fa fa-times" id="dislike" product_id="<?php echo $like['id']; ?>"></i>
                                        </a>
                                    </td>
                                </tr>
                        <?php
                            }
                        }else{
                        ?>
                            <tr>
                                <td colspan="6">Danh sách yêu thích trống <a href="index.php" class="btn btn-danger">Tiếp tục mua hàng</a></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </form>
        <div class="recommended_items">
                <!--recommended_items-->
                <h2 class="title text-center">Sản phẩm liên quan</h2>

                <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="access/images/home/recommend1.jpg" alt="" />
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="access/images/home/recommend2.jpg" alt="" />
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="access/images/home/recommend3.jpg" alt="" />
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="access/images/home/recommend1.jpg" alt="" />
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="access/images/home/recommend2.jpg" alt="" />
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="access/images/home/recommend3.jpg" alt="" />
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
    </div>
</section>