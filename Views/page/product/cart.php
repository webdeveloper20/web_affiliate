<?php

if (isset($_SESSION['cart']) && !empty($_SESSION['cart'])) {

    // echo "<pre>";
    // print_r($_SESSION['cart']);
    // echo "</pre>";
?>
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <h2 class="title text-center">Thông tin giỏ hàng</h2>
            </div>
            <form action="" method="POST" name="frm_updateCart">
                <div class="table-responsive cart_info" id="table-cart">
                    <table class="table table-condensed" id="data-table">
                        <thead>
                            <tr class="cart_menu">
                                <td class="image">Sản phẩm</td>
                                <td class="description">Tên sản phẩm</td>
                                <td class="price">Đơn giá</td>
                                <td class="quantity">Số lượng</td>
                                <td class="total">Thành tiền</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $qty = 0;
                            $_SESSION['sumPrice'] = 0;
                            foreach ($_SESSION['cart'] as $cart) {
                                $qty += (int)$cart['qty'];
                            ?>
                                <tr>
                                    <td class="cart_product">
                                        <a href=""><img style="width:110px; height:110px" src="access/images/product/<?php echo $cart['img']; ?>" alt=""></a>
                                    </td>
                                    <td class="cart_description">
                                        <h4><a href=""><?php echo $cart['name']; ?></a></h4>
                                    </td>
                                    <td class="cart_price">
                                        <p><?php echo number_format($cart['price']); ?> đ</p>
                                    </td>
                                    <td class="cart_quantity">
                                        <div class="cart_quantity_button">
                                            <!-- <a class="cart_quantity_up" href=""> + </a> -->
                                            <input class="cart_quantity_input update-cart" type="number" id="<?php echo $cart['id']; ?>" product_id="<?php echo $cart['id']; ?>" name="<?php echo $cart['name']; ?>" value="<?php echo $cart['qty']; ?>" autocomplete="off" size="2">
                                            <!-- <a class="cart_quantity_down" href=""> - </a> -->
                                        </div>
                                    </td>
                                    <td class="cart_total">
                                        <p class="cart_total_price">
                                            <?php
                                            $total = ($cart['price'] * $cart['qty']);
                                            $_SESSION['sumPrice'] += $total;
                                            echo number_format($total);
                                            ?> đ
                                        </p>
                                    </td>
                                    <td class="cart_delete">
                                        <a class="cart_quantity_delete" href="index.php?page=home&method=delCart&id=<?php echo $cart['id']; ?>">
                                            <i class="fa fa-times" onclick="return confirm('Bạn muốn loại bỏ sản phẩm này khỏi giỏ hàng ? ');"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </section>
    <!--/#cart_items-->

    <section id="do_action">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="">
                        <a class="btn btn-default check_out" href="index.php?page=home">Tiếp tục mua hàng</a>
                    </div>
                </div>
                <div class="col-sm-4" id="total-order">
                    <div class="total_area" id="subtotal">
                        <ul>
                            <li>Sub Total <span><?php echo number_format($_SESSION['sumPrice']); ?> đ</span></li>
                            <li>Shipping Cost <span>Free</span></li>
                            <li>Total <span style="color: red; font-weight: bold"><?php echo number_format($_SESSION['sumPrice']); ?> đ</span></li>
                        </ul>
                        <a class="btn btn-default check_out" href="index.php?page=home&method=checkout">Check Out</a>
                    </div>
                </div>
            </div>
            <div class="recommended_items">
                <!--recommended_items-->
                <h2 class="title text-center">Sản phẩm liên quan</h2>

                <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="access/images/home/recommend1.jpg" alt="" />
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="access/images/home/recommend2.jpg" alt="" />
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="access/images/home/recommend3.jpg" alt="" />
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="access/images/home/recommend1.jpg" alt="" />
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="access/images/home/recommend2.jpg" alt="" />
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="access/images/home/recommend3.jpg" alt="" />
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
            <!--/recommended_items-->
        </div>
    </section>
    <!--/#do_action-->
<?php
} else {
?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Thông báo: </strong> Giỏ hàng trống: <span> </span>
        <!-- <a href="index.php?page=home&method=shop">Mua hàng</a> -->
        <a href="index.php?page=home">Mua hàng</a>
    </div>
<?php
}
?>