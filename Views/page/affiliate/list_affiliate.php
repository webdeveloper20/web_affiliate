<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li><a href="index.php?page=affiliate&method=listAffiliate">Danh sách chương trình</a></li>
                <?php
                if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id'])) {
                ?>
                    <li><a href="index.php?page=affiliate&method=list_aff_member&affiliate_id=<?php if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id'])) {
                                                                                                    echo $_SESSION['aff_member_id'];
                                                                                                } ?>">Chương trình bạn đã đăng ký</a></li>
                    <li><a href="index.php?page=affiliate&method=listSaleAffDetail&affiliate_id=<?php if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id'])) {
                                                                                                    echo $_SESSION['aff_member_id'];
                                                                                                } ?>">Kết quả bán hàng</a></li>
                    <li><a href="index.php?page=affiliate&method=logout&affiliate_id=<?php if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id'])) {
                                                                                            echo $_SESSION['aff_member_id'];
                                                                                        } ?>">Đăng xuất</a></li>

                <?php
                }else{
                ?>
                    <li><a href="index.php?page=affiliate&method=login">Đăng nhập</a></li>
                <?php
                }
                ?>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            <?php
            if (isset($_SESSION['check']) && $_SESSION['check'] == 1) {
            ?>
                <div class="alert alert-warning checkAccAffiliate" role="alert">
                    Đăng nhập thành công
                </div>
            <?php
                unset($_SESSION['check']);
            }
            ?>
            <table class="table table-condensed text-center">
                <thead>
                    <tr class="cart_menu">
                        <td>#</td>
                        <td>Danh sách chương trình</td>
                        <td>Mô tả</td>
                        <td>Hoa hồng</td>
                        <td>Trạng thái</td>
                        <td>Tùy chọn </td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $stt = 1;
                    foreach ($listAffiliate as $aff) {
                    ?>
                        <tr>
                            <td><?= $stt++; ?></td>
                            <td class="cart_description">
                                <?= $aff['title'] ?>
                            </td>
                            <td class="cart_price">
                                <?= $aff['description'] ?>
                            </td>
                            <td style="color:red;" class="cart_quantity">
                                <?php if ($aff['category'] == 2) {
                                    echo  $aff['rose'] . "%";
                                } elseif ($aff['category'] == 1) {
                                    echo  number_format($aff['rose'], 0, ',', '.') . " VNĐ";
                                } ?>
                            </td>
                            <td style="color:green;" class="cart_quantity">
                                <?php if ($aff['status'] == 1) {
                                    echo "Đang diễn ra";
                                } else {
                                    echo "Đã ngưng";
                                } ?>
                            </td>
                            <td>
                                <a href="index.php?page=affiliate&method=addAffiliate&id_pro=<?= $aff['program_id'] ?>&id_aff=<?php if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id'])) {
                                                                                                                                    echo $_SESSION['aff_member_id'];
                                                                                                                                } ?>" class="btn btn-info get">Đăng ký</a>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>
</section>
<!--/#cart_items-->
<?php
// echo "<pre>";
// print_r($listAffiliate);
// echo "</pre>";
?>