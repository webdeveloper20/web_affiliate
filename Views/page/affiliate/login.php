<div class="container">
    <div class="row">
        <div class="col-md-9" style="margin:30px auto; padding-left:400px;">
            <div class="login-form">
                <h2>Đăng nhập tài khoản affiliate</h2>
                <?php
                if (isset($_SESSION['check']) && $_SESSION['check'] == 1) {
                ?>
                    <div class="alert alert-warning checkAccAffiliate" role="alert">
                        Đăng ký thành công
                    </div>
                <?php
                    unset($_SESSION['check']);
                } elseif (isset($_SESSION['check']) && $_SESSION['check'] == 2) {
                ?>
                    <div class="alert alert-warning checkAccAffiliate" role="alert">
                        Thông tin tài khoản hoặc mật khẩu không chính xác
                    </div>
                <?php
                    unset($_SESSION['check']);
                } elseif (isset($_SESSION['check']) && $_SESSION['check'] == 3) {
                ?>
                    <div class="alert alert-warning checkAccAffiliate" role="alert">
                        Bạn cần đăng nhập để đăng ký chương trình
                    </div>
                <?php
                    unset($_SESSION['check']);
                }
                ?>
                <form action="" method="POST">
                    <input type="email" name="email" placeholder="Email" />
                    <input type="password" name="password" placeholder="Password" />
                    <div style="display: flex; flex-direction: row;">
                        <button type="submit" name="login_aff" class="btn btn-default">Đăng nhập</button>
                        <a href="index.php?page=affiliate&method=register" style="margin-left: 20px;" class="btn btn-default get">Đăng ký</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>