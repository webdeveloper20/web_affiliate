<?php
if (isset($_SESSION['check']) && $_SESSION['check'] == 4) {
?>
    <script>
        alert('Bạn đã đăng ký chương trình này!!');
    </script>
<?php
    unset($_SESSION['check']);
}
if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id']) && isset($_GET['affiliate_id']) && !empty($_GET['affiliate_id'])) {
?>
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="index.php?page=affiliate&method=listAffiliate">Danh sách chương trình</a></li>
                    <?php
                if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id'])) {
                ?>
                    <li><a href="index.php?page=affiliate&method=list_aff_member&affiliate_id=<?php if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id'])) {
                                                                                                    echo $_SESSION['aff_member_id'];
                                                                                                } ?>">Chương trình bạn đã đăng ký</a></li>
                    <li><a href="index.php?page=affiliate&method=listSaleAffDetail&affiliate_id=<?php if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id'])) {
                                                                                                    echo $_SESSION['aff_member_id'];
                                                                                                } ?>">Kết quả bán hàng</a></li>
                    <li><a href="index.php?page=affiliate&method=logout&affiliate_id=<?php if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id'])) {
                                                                                            echo $_SESSION['aff_member_id'];
                                                                                        } ?>">Đăng xuất</a></li>

                <?php
                }
                ?>
                </ol>
            </div>
            <div class="table-responsive cart_info">
                <table class="table table-condensed text-center">
                    <thead>
                        <tr class="cart_menu">
                            <td>#</td>
                            <td>Chương trình bạn đăng ký</td>
                            <td>Hoa hồng</td>
                            <td>Trạng thái</td>
                            <td>Link</td>
                            <td>Tùy chọn</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $stt = 1;
                        if (!empty($listAffMember)) {
                            foreach ($listAffMember as $aff) {
                        ?>
                                <tr>
                                    <td><?= $stt++; ?></td>
                                    <td class="cart_description">
                                        <?= $aff['title'] ?>
                                    </td>
                                    <td style="color:red;" class="cart_quantity">
                                        <?php if ($aff['category'] == 2) {
                                            echo  $aff['rose'] . "%";
                                        } elseif ($aff['category'] == 1) {
                                            echo  number_format($aff['rose'], 0, ',', '.') . " VNĐ";
                                        } ?>
                                    </td>
                                    <td style="color:red;" class="cart_quantity">
                                        <?php if ($aff['status'] == 1) {
                                            echo "online";
                                        } else {
                                            echo "Chờ phê duyệt";
                                        } ?>
                                    </td>
                                    <td class="cart_price">
                                        <?php if ($aff['status'] == 1) {
                                            echo "index.php?page=home&affiliate=" . $aff['affiliate_code'];
                                        } else {
                                            echo "......";
                                        } ?>
                                    </td>
                                    <td>
                                        <a href="index.php?page=affiliate&method=list_aff_member&affiliate_id=<?= $_GET['affiliate_id'] ?>&stop=<?= $aff['id'] ?>" class="btn btn-info get">Hủy bỏ</a>
                                    </td>
                                </tr>
                        <?php
                            }
                        }else{
                        ?>
                            <tr>
                                <td colspan="6" style="padding-top:30px;">Bạn chưa đăng ký chương trình nào
                                    <a href="index.php?page=affiliate&method=listAffiliate" class="btn btn-danger">Đăng ký ngay</a>
                                </td>
                            </tr>
                        <?php
                        }

                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!--/#cart_items-->
<?php
} else {
    header('Location:index.php?page=affiliate&method=login');
}
?>

<?php
// echo "<pre>";
// print_r($listAffMember);
// echo "</pre>";
?>