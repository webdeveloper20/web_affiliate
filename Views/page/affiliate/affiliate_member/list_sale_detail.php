<?php
if (isset($_SESSION['check']) && $_SESSION['check'] == 4) {
?>
    <script>
        alert('Bạn đã đăng ký chương trình này!!');
    </script>
<?php
    unset($_SESSION['check']);
}
if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id']) && isset($_GET['affiliate_id']) && !empty($_GET['affiliate_id'])) {
?>
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="index.php?page=affiliate&method=listAffiliate">Danh sách chương trình</a></li>
                    <?php
                if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id'])) {
                ?>
                    <li><a href="index.php?page=affiliate&method=list_aff_member&affiliate_id=<?php if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id'])) {
                                                                                                    echo $_SESSION['aff_member_id'];
                                                                                                } ?>">Chương trình bạn đã đăng ký</a></li>
                    <li><a href="index.php?page=affiliate&method=listSaleAffDetail&affiliate_id=<?php if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id'])) {
                                                                                                    echo $_SESSION['aff_member_id'];
                                                                                                } ?>">Kết quả bán hàng</a></li>
                    <li><a href="index.php?page=affiliate&method=logout&affiliate_id=<?php if (isset($_SESSION['aff_member_id']) && !empty($_SESSION['aff_member_id'])) {
                                                                                            echo $_SESSION['aff_member_id'];
                                                                                        } ?>">Đăng xuất</a></li>

                <?php
                }
                ?>
            </ol>
            </div>
            <div class="table-responsive cart_info">
                <table class="table table-condensed text-center">
                    <thead>
                        <tr class="cart_menu">
                            <th>STT</th>
                            <th>Tên khách hàng</th>
                            <th>Tên sản phẩm</th>
                            <th>Đơn giá</th>
                            <th>Số lượng</th>
                            <th>Tổng tiền</th>
                            <th>Tên chương trình</th>
                            <th>Hoa hồng</th>
                            <th>Tiền hoa hồng</th>
                            <th>Trạng thái</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $stt = 1;
                        $moneyReceived = $moneyNotReceived = 0;
                        if (!empty($listSaleAffDetail)) {
                            foreach ($listSaleAffDetail as $listSale) {
                        ?>
                                <tr>
                                    <td><?= $stt++ ?></td>
                                    <td><?= $listSale['nameMember'] ?></td>
                                    <td><?= $listSale['nameProduct'] ?></td>
                                    <td><?= $listSale['price'] ?></td>
                                    <td><?= $listSale['quantity'] ?></td>
                                    <td><?= $listSale['total'] ?></td>
                                    <td><?= $listSale['nameProgram'] ?></td>
                                    <td style="color:blue;"> <?php if ($listSale['category'] == 2) {
                                                                    echo  $listSale['rose'] . "%";
                                                                } elseif ($listSale['category'] == 1) {
                                                                    echo  number_format($listSale['rose'], 0, ',', '.') . " VNĐ";
                                                                } ?></td>
                                    <td style="color:red;"><?php if ($listSale['category'] == 2) {
                                                                $totalOne = $listSale['total'] / 100 * $listSale['rose'];
                                                                echo number_format($totalOne, 0, ',', '.') . " VNĐ";
                                                            } elseif ($listSale['category'] == 1) {
                                                                $totalOne = $listSale['rose'];
                                                                echo  number_format($totalOne, 0, ',', '.') . " VNĐ";
                                                            } ?></td>
                                    <?php if ($listSale['status'] == 0) {
                                    ?>
                                        <td style="color:blue;">Chờ xác nhận</td>
                                    <?php
                                    } else {
                                    ?>
                                        <td style="color:green;">đã xác nhận</td>
                                    <?php
                                    } ?>

                                </tr>
                            <?php
                                if ($listSale['status'] == 0) {
                                    $moneyNotReceived += $totalOne;
                                } else {
                                    $moneyReceived += $totalOne;
                                }
                            }
                            ?>
                            <tr>
                                <td colspan="7" style="color:green;">Tổng tiền đã nhận
                                </td>
                                <td colspan="3" style="color:red; font-weight:bold;"><?php echo number_format($moneyReceived, 0, ',', '.') . " VNĐ"; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" style="color:green;">
                                    Tồng tiền chưa nhận
                                </td>
                                <td colspan="3" style="color:red; font-weight:bold;"><?php echo number_format($moneyNotReceived, 0, ',', '.') . " VNĐ"; ?>
                                </td>
                            </tr>
                        <?php
                        }else{
                        ?>
                            <tr>
                                <td colspan="10" style="padding-top:30px;">Bạn chưa bán được sản phẩm nào</td>
                            </tr>
                        <?php
                        }
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!--/#cart_items-->
<?php
} else {
    header('Location:index.php?page=affiliate&method=login');
}
?>

<?php
// echo "<pre>";
// print_r($listAffMember);
// echo "</pre>";
?>