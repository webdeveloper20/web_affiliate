<div class="container">
    <div class="row">
        <div class="col-md-9" style="margin:30px auto; padding-left:400px;">
            <div class="signup-form">
                <!--sign up form-->
                <h2>Tạo tài khoản mới</h2>
                <?php
                if (isset($_SESSION['check']) && $_SESSION['check'] == 2) {
                ?>
                    <div class="alert alert-warning checkAccAffiliate" role="alert">
                        Email đã được đăng ký vui lòng chọn email khác
                    </div>
                <?php
                    unset($_SESSION['check']);
                }
                ?>

                <form action="" method="POST" name="register" onsubmit="return FormValidate();">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="name">Họ tên : <span style="color: red;" id="errorName">(*)</span></label>
                            <input type="text" name="name" class="form-control" id="name" />
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="phone">Số điện thoại : <span style="color: red;" id="erorPhone">(*)</span></label>
                            <input type="text" name="phone" class="form-control" id="phone" />
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="address">Địa chỉ :<span style="color: red;" id="errorAddress">(*)</span></label>
                            <input type="text" name="address" class="form-control" id="address" />
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="email">Email :<span style="color: red;" id="erorEmail">(*)</span></label>
                            <input type="text" name="email" class="form-control" id="email" />
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="pass">Mật khẩu :<span style="color: red;" id="errorPass">(*)</span></label>
                            <input type="password" name="password" class="form-control" id="pass" />
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <button type="submit" name="register_aff" class="btn btn-primary">Gửi thông tin</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>