-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2021 at 02:56 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_affiliate`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_affiliate`
--

CREATE TABLE `tbl_affiliate` (
  `affiliate_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_affiliate`
--

INSERT INTO `tbl_affiliate` (`affiliate_id`, `name`, `email`, `phone`, `password`, `address`, `status`, `created_at`) VALUES
(1, 'Vũ Đông Anh', 'donganhvu16@gmail.com', '0832911010', '20102000', 'Hà Nam', 1, '2020-12-24 03:21:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cate`
--

CREATE TABLE `tbl_cate` (
  `id_cate` int(11) NOT NULL,
  `name_cate` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  `stt_cate` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_cate`
--

INSERT INTO `tbl_cate` (`id_cate`, `name_cate`, `create_at`, `stt_cate`) VALUES
(5, 'Quần áo mùa Đông', '2019-11-21 19:33:56', 1),
(6, 'Quần áo mùa Hè', '2019-11-21 19:34:07', 1),
(7, 'Quần áo mùa Thu', '2019-11-21 19:34:17', 1),
(8, 'Quần áo Mùa Xuân', '2019-11-21 19:34:26', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_order`
--

CREATE TABLE `tbl_detail_order` (
  `id_order` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float NOT NULL,
  `total` float NOT NULL,
  `stt` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_detail_order`
--

INSERT INTO `tbl_detail_order` (`id_order`, `id_product`, `quantity`, `price`, `total`, `stt`) VALUES
(40, 13, 1, 9500000, 9500000, 1),
(40, 15, 1, 9500000, 9500000, 1),
(41, 10, 1, 1250000, 1250000, 1),
(41, 14, 1, 9500000, 9500000, 1),
(42, 11, 1, 5500000, 5500000, 1),
(42, 15, 2, 9500000, 19000000, 1),
(43, 10, 1, 1250000, 1250000, 1),
(43, 14, 3, 9500000, 28500000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_images`
--

CREATE TABLE `tbl_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `img` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  `update_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member`
--

CREATE TABLE `tbl_member` (
  `id_member` int(11) NOT NULL,
  `name_member` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone_member` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `addres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_member` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `stt_member` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_member`
--

INSERT INTO `tbl_member` (`id_member`, `name_member`, `phone_member`, `addres`, `email_member`, `password`, `stt_member`) VALUES
(43, 'Nguyễn Văn A', '0123456789', 'Hà Tĩnh', 'nguyenvana@gmail.com', '730a4aad096e2a0edce0cfe1fd644b42', 1),
(44, 'Nguyễn Văn D', '0987789987', 'Hải Phòng', 'vnd@gmail.com', '730a4aad096e2a0edce0cfe1fd644b42', 1),
(45, 'Lê Thì Hạnh', '0123456789', 'HCM', 'hanhle@gmail.com', '730a4aad096e2a0edce0cfe1fd644b42', 1),
(46, 'Lê Thị Hương', '0123456789', 'Hải Phòng', 'huongle@gmail.com', '730a4aad096e2a0edce0cfe1fd644b42', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `id_order` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `note` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `stt_order` int(11) NOT NULL DEFAULT 1,
  `date_order` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`id_order`, `id_member`, `note`, `stt_order`, `date_order`) VALUES
(40, 43, '', 1, '2021-01-05 15:48:50'),
(41, 44, '', 1, '2021-01-05 15:56:09'),
(42, 45, '', 1, '2021-01-05 16:35:00'),
(43, 46, '', 1, '2021-01-05 19:56:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_referal`
--

CREATE TABLE `tbl_order_referal` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `referal_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `create_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_order_referal`
--

INSERT INTO `tbl_order_referal` (`id`, `order_id`, `referal_id`, `status`, `create_at`) VALUES
(3, 43, 11, 0, '2021-01-05 19:56:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL,
  `id_cate` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` tinyint(4) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Mô tả sản phẩm',
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  `stt` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `id_cate`, `name`, `price`, `img`, `quantity`, `description`, `slug`, `create_at`, `stt`) VALUES
(9, 6, 'Váy Body V01', 850000, 'products-01.png', 10, 'Mô tả sản phẩm Váy Body V01', '', '2019-11-05 19:38:02', 1),
(10, 6, 'Váy Body V02', 1250000, 'products-02.png', 5, 'Mô tả sản phẩm Váy Body V02', '', '2019-11-21 19:38:02', 2),
(11, 7, 'Đầm dạ hội DV01', 5500000, 'products-03.png', 5, 'Mô tả sản phẩm Váy Body DV01', '', '2019-11-13 19:38:02', 1),
(12, 8, 'Đầm dạ hội DV02', 6500000, 'products-04.png', 7, 'Mô tả sản phẩm Váy Body DV02', '', '2019-11-21 19:38:02', 1),
(13, 6, 'Váy xòe VX01', 9500000, 'products-05.png', 7, 'Mô tả sản phẩm Váy Body VX01', '', '2019-11-21 19:38:02', 2),
(14, 7, 'Váy xòe VX02', 9500000, 'products-06.png', 7, 'Mô tả sản phẩm Váy Body VX02', '', '2019-11-21 19:38:02', 2),
(15, 7, 'Váy dạ hội VDH01', 9500000, 'products-07.png', 7, 'Mô tả sản phẩm Váy Body VDH01', '', '2019-11-21 19:38:02', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_program`
--

CREATE TABLE `tbl_program` (
  `program_id` int(11) NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rose` tinyint(4) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_program`
--

INSERT INTO `tbl_program` (`program_id`, `title`, `rose`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Ngày hội mùa xuân', 5, 'Mô tả ngày hội', 1, '2020-12-18 20:28:09', '2020-12-18 20:28:09'),
(2, 'Lễ hội mùa thu', 10, 'lễ hội vui vẻ', 1, '2020-12-18 20:41:29', '2020-12-18 20:41:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_referal`
--

CREATE TABLE `tbl_referal` (
  `id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `affiliate_code` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_referal`
--

INSERT INTO `tbl_referal` (`id`, `affiliate_id`, `program_id`, `affiliate_code`, `status`, `created_at`) VALUES
(11, 1, 1, '1.1.o1S6NjlCxuEAds2c', 1, '2021-01-05 19:47:50');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `phone`, `email`, `password`, `status`) VALUES
(1, 'Vũ Đông Anh', '0832911020', 'admin@gmail.com', '49992f7303c2ae794b5c274ce3f54dba', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_affiliate`
--
ALTER TABLE `tbl_affiliate`
  ADD PRIMARY KEY (`affiliate_id`);

--
-- Indexes for table `tbl_cate`
--
ALTER TABLE `tbl_cate`
  ADD PRIMARY KEY (`id_cate`);

--
-- Indexes for table `tbl_detail_order`
--
ALTER TABLE `tbl_detail_order`
  ADD PRIMARY KEY (`id_order`,`id_product`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `tbl_images`
--
ALTER TABLE `tbl_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_id_to_tbl_product` (`product_id`);

--
-- Indexes for table `tbl_member`
--
ALTER TABLE `tbl_member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_member` (`id_member`);

--
-- Indexes for table `tbl_order_referal`
--
ALTER TABLE `tbl_order_referal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_referal_id_to_tbl_referal` (`referal_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cate` (`id_cate`);

--
-- Indexes for table `tbl_program`
--
ALTER TABLE `tbl_program`
  ADD PRIMARY KEY (`program_id`);

--
-- Indexes for table `tbl_referal`
--
ALTER TABLE `tbl_referal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `affiliate_id_to_tbl_affiliate` (`affiliate_id`),
  ADD KEY `program_id_to_tbl_program` (`program_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_affiliate`
--
ALTER TABLE `tbl_affiliate`
  MODIFY `affiliate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_cate`
--
ALTER TABLE `tbl_cate`
  MODIFY `id_cate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_images`
--
ALTER TABLE `tbl_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_member`
--
ALTER TABLE `tbl_member`
  MODIFY `id_member` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `tbl_order_referal`
--
ALTER TABLE `tbl_order_referal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_program`
--
ALTER TABLE `tbl_program`
  MODIFY `program_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_referal`
--
ALTER TABLE `tbl_referal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_detail_order`
--
ALTER TABLE `tbl_detail_order`
  ADD CONSTRAINT `fk_id_order_detail_order_order` FOREIGN KEY (`id_order`) REFERENCES `tbl_order` (`id_order`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_product_order_detail_product` FOREIGN KEY (`id_product`) REFERENCES `tbl_product` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `tbl_images`
--
ALTER TABLE `tbl_images`
  ADD CONSTRAINT `fk_product_id_to_tbl_product` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD CONSTRAINT `fk_id_member_order_member` FOREIGN KEY (`id_member`) REFERENCES `tbl_member` (`id_member`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `tbl_order_referal`
--
ALTER TABLE `tbl_order_referal`
  ADD CONSTRAINT `fk_referal_id_to_tbl_referal` FOREIGN KEY (`referal_id`) REFERENCES `tbl_referal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD CONSTRAINT `fk_id_cate_tbl_product_cate` FOREIGN KEY (`id_cate`) REFERENCES `tbl_cate` (`id_cate`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `tbl_referal`
--
ALTER TABLE `tbl_referal`
  ADD CONSTRAINT `affiliate_id_to_tbl_affiliate` FOREIGN KEY (`affiliate_id`) REFERENCES `tbl_affiliate` (`affiliate_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `program_id_to_tbl_program` FOREIGN KEY (`program_id`) REFERENCES `tbl_program` (`program_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
